package com.whatshere;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.whatshere.http.HttpClient;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyLog;

import java.util.Calendar;

import dmax.dialog.SpotsDialog;
import grx.tracking.core.events.EventFindTasks;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.core.transport.RouteResultMessage;

class FindTasksDelegator extends TaskDelegator {

    private static long TIME_OUT = 3 * 60 * 1000L;

    private RouteInfoActivity routeInfoActivity;
    private SpotsDialog progressDialog;
    private TruckId truckId;
    private boolean startBase;
    private UserId userId;
    private String sector_id;
    private long started_time;
    private String status;


    public FindTasksDelegator(RouteInfoActivity routeInfoActivity, UserId userId, boolean startBase, TruckId truckId, String sector_id) {
        this.routeInfoActivity = routeInfoActivity;
        this.userId = userId;
        this.startBase = startBase;
        this.truckId = truckId;
        this.sector_id = sector_id;
        this.progressDialog = new SpotsDialog(routeInfoActivity, R.style.CustomProgressOrders);
        this.progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.started_time = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    public void onPreExecute() {
        this.progressDialog.show();
    }

    @Override
    public void doInBackground() throws Exception {

        EventFindTasks start = new EventFindTasks();
        start.setFromUserId(this.userId);
        start.setTruckId(this.truckId);
        start.setStartBase(this.startBase);
        start.setDateEvent(Calendar.getInstance().getTime());
        start.setDeliverCapacity(5);
        start.setPickupCapacity(2);
        start.setConfirmRoute(false);
        start.setSector_Id(this.sector_id);

        start.setType(GRXEvent.EVENT_TYPE.REAL_TIME);
        start.setTo(GRXEvent.TO_TYPE.TO_USER);

        JSONConverter conv = new JSONConverter();

        EventMessage message = new EventMessage();
        message.setEventClass(EventFindTasks.class.getCanonicalName());
        message.setJsonString(conv.toJson(start));

        HttpClient.getRef().sendEvent(message);

        while (routeInfoActivity.getRouteInfo() == null || routeInfoActivity.getOrdersInfo() == null) {
            if (routeInfoActivity.getErrorResult() != null) {
                RouteResultMessage error = routeInfoActivity.getErrorResult();
                throw new Exception(error.getMessage());
            }

            if (routeInfoActivity.getWatingMessage() != null) {
                this.status = "waiting_route";
                return;
            }

            if ((Calendar.getInstance().getTimeInMillis() - started_time) > TIME_OUT) {
                this.status = "timeout";
                return;
            }

            Thread.sleep(1000);
        }

        this.status = "route_found";
    }


    @Override
    public void onPostExecute() {
        this.progressDialog.dismiss();

        if (super.taskException != null) {
            MyLog.log(super.taskException);
            MyDialogs.showError(routeInfoActivity, "Procurando pedidos", super.taskException.getLocalizedMessage());
        } else {
            if ("waiting_route".equals(this.status)) {
                MyDialogs.showInformation(routeInfoActivity, "Procurando pedidos", "Aguardando confirmação da Rota", new MyDialogs.MyDialogListener() {
                    @Override
                    public void processClose() {
                        routeInfoActivity.showMessage("Aguardando confirmação da Rota");
                    }
                });

            }

            if ("timeout".equals(this.status)) {
                MyDialogs.showInformation(routeInfoActivity, "Procurando pedidos", "Não foi possivel encontrar um rota - Tempo de espera ultrapassado", new MyDialogs.MyDialogListener() {
                    @Override
                    public void processClose() {

                    }
                });

            }

            if ("route_found".equals(this.status)) {
                routeInfoActivity.updateViews();
            }

        }
    }
}

package com.whatshere;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

import com.whatshere.custom.CustomActivity;

import grx.tracking.core.persistence.OrderInfo;

/**
 * The OrderDetail is the activity class that shows details about a selected
 * Order. This activity only shows dummy detail text and Image, you need to load
 * and display actual contents.
 */
public class OrderDetail extends CustomActivity {

    private OrderInfo orderInfo;

    /* (non-Javadoc)
     * @see com.food.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_details);

        if (getIntent().hasExtra("order")) {
            this.orderInfo = (OrderInfo) getIntent().getSerializableExtra("order");
        }

        getActionBar().setTitle("Pedido: " + orderInfo.getOrderId().toString());
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_theme)));

        String type = "";

        if (this.orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK)) {
            type = "Retirada";
        }

        if (this.orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.DELIVERY)) {
            type = "Entrega";
        }

        if (this.orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE)) {
            type = "Troca";
        }

        ((TextView) this.findViewById(R.id.textViewOrderId)).setText("Pedido: " + this.orderInfo.getOrderId());
        ((TextView) this.findViewById(R.id.textViewAddress)).setText(this.orderInfo.getAddress().getAddress().asAddressString());
        ((TextView) this.findViewById(R.id.textViewType)).setText(type);

    }

}

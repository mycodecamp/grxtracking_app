package com.whatshere.eventbus;

import com.google.common.eventbus.EventBus;

public class EventBusIntance {

    private static EventBus eventBus;

    public static EventBus getEventBus() {
        if (eventBus == null) {
            eventBus = new EventBus();
        }
        return eventBus;
    }
}

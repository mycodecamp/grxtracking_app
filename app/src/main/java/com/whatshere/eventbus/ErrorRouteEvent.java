package com.whatshere.eventbus;

import java.io.Serializable;

import grx.tracking.core.transport.RouteResultMessage;

public class ErrorRouteEvent implements Serializable {
    public RouteResultMessage messageError;
}

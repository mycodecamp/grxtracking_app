package com.whatshere.eventbus;

import com.google.android.gms.location.LocationResult;

import java.io.Serializable;

import grx.tracking.core.persistence.UserId;

public class LocationEvent implements Serializable {
    public LocationResult result;
    public UserId userId;
}

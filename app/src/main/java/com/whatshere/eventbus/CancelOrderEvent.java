package com.whatshere.eventbus;

import java.io.Serializable;

import grx.tracking.core.persistence.OrderId;

public class CancelOrderEvent implements Serializable {
    public OrderId order_id;
}

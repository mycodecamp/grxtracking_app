package com.whatshere.eventbus;

import java.io.Serializable;

import grx.tracking.core.transport.RouteResultMessage;

public class WaitingRouteEvent implements Serializable {
    public RouteResultMessage message;
    public String status;
}

package com.whatshere.eventbus;

import java.util.List;

import grx.tracking.core.persistence.OrderInfo;

public class OrdersReceivedEvent {

    public List<OrderInfo> orders;
}

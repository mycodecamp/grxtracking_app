package com.whatshere.eventbus;

import java.io.Serializable;

import grx.tracking.core.persistence.OrderInfo;

public class RemoveOrderEvent implements Serializable{

    public OrderInfo order;
    public boolean notifyToServer;
    public boolean wasForProximity = false;

}

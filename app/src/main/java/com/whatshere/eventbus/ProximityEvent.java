package com.whatshere.eventbus;

import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;

public class ProximityEvent {
    public OrderInfo order;
    public GeoLocation location;
    public boolean bringToFront = false;
}

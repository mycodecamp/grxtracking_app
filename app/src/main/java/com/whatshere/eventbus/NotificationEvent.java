package com.whatshere.eventbus;

import java.io.Serializable;

public class NotificationEvent implements Serializable {

    public String route_id;
    public String message;
}

package com.whatshere.tasks;

public abstract class TaskDelegator {

    protected Exception taskException;
    protected boolean isFinished;

    public abstract void doInBackground() throws Exception;

    public void onPostExecute() {

    }

    public boolean isFinished() {
        return isFinished;
    }

    public void onPreExecute() {
    }
}

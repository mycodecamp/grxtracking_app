package com.whatshere.tasks;

import android.os.AsyncTask;

import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.List;

public class ExecuteTask {

    // private static List<AsyncTask> tasks = new ArrayList<>();


    public static void executeTask(final TaskDelegator delegator) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                delegator.isFinished = false;
                try {
                    delegator.doInBackground();
                } catch (Exception e) {
                    delegator.taskException = e;
                } catch (Throwable e) {
                    delegator.taskException = new Exception(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                delegator.onPostExecute();
                delegator.isFinished = true;
            }

            @Override
            protected void onPreExecute() {
                try {
                    delegator.onPreExecute();
                } catch (Exception e) {
                    MyLog.log(e);
                } catch (Throwable e) {
                    MyLog.log(new Exception(e));
                }
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        // tasks.add(task);
    }
}

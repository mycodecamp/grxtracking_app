package com.whatshere;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.whatshere.custom.CustomActivity;
import com.whatshere.dialogs.TruckSelectionDialog;
import com.whatshere.http.HttpClient;
import com.whatshere.persistence.DAO;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyLog;

import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.Credentials;


/**
 * The Activity Login is launched after the Splash screen. You need to write
 * your logic for actual Login.
 */
public class Login extends CustomActivity {

    private DAO dao;

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dao = new DAO();

        setContentView(R.layout.login);

        setupView();

        final EditText editTextUserName = this.findViewById(R.id.editTextUserName);
        final EditText editTextPassword = this.findViewById(R.id.editTextPassword);

        Button btnLogin = this.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    final HttpClient client = HttpClient.getRef();

                    final Credentials credentials = new Credentials();
                    credentials.setUsername(editTextUserName.getText().toString());
                    credentials.setPassword(editTextPassword.getText().toString());
                    credentials.setApp_version("v1.0.6");

                    ExecuteTask.executeTask(new TaskDelegator() {
                        @Override
                        public void doInBackground() throws Exception {
                            UserInfo result = client.loginWithUser(credentials, dao);
                            result.setRoles(null);

                            dao.saveObj("userinfo", result);

                            Intent intent = new Intent(Login.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            Bundle extras = new Bundle();
                            extras.putSerializable("userinfo", result);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtras(extras);

                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onPostExecute() {
                            if (taskException != null) {
                                MyLog.log(taskException);
                                MyDialogs.showError(Login.this, "Erro no login", taskException.getLocalizedMessage());
                            }
                        }
                    });


                } catch (Exception e) {
                    MyLog.log(e);
                    MyDialogs.showError(Login.this, "Erro no login", e.getMessage());
                }
            }


        });


    }

    /**
     * Setup the click & other events listeners for the view components of this
     * screen. You can add your logic for Binding the data to TextViews and
     * other views as per your need.
     */
    private void setupView() {
        Button b = (Button) setTouchNClick(R.id.btnReg);

        setTouchNClick(R.id.btnLogin);
        setTouchNClick(R.id.btnForget);
    }

    /* (non-Javadoc)
     * @see com.taxi.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnLogin) {
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}

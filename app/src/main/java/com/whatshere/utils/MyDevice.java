package com.whatshere.utils;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class MyDevice {

    public static String getDeviceId(Activity activity) {
        try {
            Context context = activity.getApplicationContext();
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String uuid;
            String androidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            String deviceID = tm.getDeviceId();
            String simID = tm.getSimSerialNumber();

            if ("9774d56d682e549c".equals(androidID) || androidID == null) {
                androidID = "";
            }

            if (deviceID == null) {
                deviceID = "";
            }

            if (simID == null) {
                simID = "";
            }

            uuid = androidID + deviceID + simID;
            uuid = String.format("%32s", uuid).replace(' ', '0');
            uuid = uuid.substring(0, 32);
            uuid = uuid.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");

            return uuid;

        } catch (SecurityException e) {
            MyLog.log(e);
        }

        return null;
    }
}

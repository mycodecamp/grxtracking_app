package com.whatshere.utils;

import android.util.Log;

public class MyLog {

    private static String TAG = "grx.tracking";

    public static void log(Exception e) {
        Log.w(TAG, e.getLocalizedMessage(), e);
    }

    public static void log(Throwable t) {
        Log.w(TAG, t.getLocalizedMessage(), t);
    }

    public static void i(String message) {
        Log.i(TAG, message);
    }

}

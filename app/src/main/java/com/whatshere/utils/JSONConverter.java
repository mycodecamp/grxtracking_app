package com.whatshere.utils;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.PermissionDescr;

public class JSONConverter {

    private Gson gson;

    public JSONConverter() {
        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                try {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                } catch (Exception e) {
                    return Calendar.getInstance().getTime();
                }
            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                return src == null ? null : new JsonPrimitive(src.getTime());
            }
        });

        builder.registerTypeAdapter(Serializable.class, new JsonDeserializer<List<PermissionDescr>>() {
            public List<PermissionDescr> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                return (List) context.deserialize(json, List.class);
            }
        });

        builder.registerTypeAdapter(Address.class, new JsonDeserializer<Address>() {
            @Override
            public Address deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                Address address = new Address();
                if (json.isJsonObject()) {
                    JsonObject jobj = (JsonObject) json;
                    address.setEndereco(getMemberAsString(jobj, "endereco"));
                    address.setXBairro(getMemberAsString(jobj, "xbairro"));
                    address.setNro(getMemberAsString(jobj, "nro"));
                    address.setXMun(getMemberAsString(jobj, "xmun"));
                }
                return address;
            }
        });


        this.gson = builder.create();
    }

    private String getMemberAsString(JsonObject obj, String member) {
        try {
            if (obj.has(member)) {
                JsonElement mm = obj.get(member);
                String str = mm.getAsString();
                return str != null ? str.trim() : str;
            }
        } catch (Exception e) {

        }
        return null;
    }

    public String toJson(Object obj) throws Exception {
        return this.gson.toJson(obj);
    }

    public <T> T toObject(String json, Class<T> classs) throws Exception {
        return this.gson.fromJson(json, classs);
    }


}

package com.whatshere.utils;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.whatshere.R;


public class MyDialogs {


    public static void showError(Activity context, String title, String message) {

        if (context.isFinishing()) {
            return;
        }

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleRed).create();
        dialog.setTitle(title);

        message = formatMessage(message);
        dialog.setMessage(message);

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static AlertDialog showConfirm(Activity context, String title, String message, final Callback callback) {
        if (context.isFinishing()) {
            return null;
        }

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue).setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (callback != null) {
                    callback.processNo();
                }
            }
        }).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (callback != null) {
                    callback.processYes();
                }
            }

        }).create();

        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        return dialog;
    }

    public static AlertDialog showAskLocation(Context context, String title, String message, final Callback callback) {
        AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue).setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (callback != null) {
                    callback.processNo();
                }

            }
        }).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (callback != null) {
                    callback.processYes();
                }
            }

        }).create();


        dialog.setContentView(R.layout.list);
        ListView list = dialog.findViewById(R.id.list);


        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        return dialog;
    }

    public static interface MyDialogListener {
        public void processClose();
    }

    public static void showInformation(Activity context, String title, String message, final MyDialogListener listener) {
        if (context.isFinishing()) {
            return;
        }

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) {
                            listener.processClose();
                        }
                    }

                }).create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static void showInformationCenter(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue);


        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }

        });
        AlertDialog dialog = builder.show();

        TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }

    public static interface Callback {
        public void processYes();

        public void processNo();
    }

    private static String formatMessage(String message) {
        if (message == null) {
            message = "";
        }

        if (message.contains("null")) {
            message = message.replace("null", "");
            message = message.replace("Erro:", "Erro desconhecido");
        }
        return message;
    }
}

package com.whatshere.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


import com.whatshere.RouteInfoActivity;

import java.util.List;

public class PackagesUtils {
    public static boolean isPackageInstalled(String packagename, Activity activity) {
        try {
            PackageManager packageManager = activity.getPackageManager();
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(packageName);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 199;

    public static void checkPermission(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.KILL_BACKGROUND_PROCESSES)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.KILL_BACKGROUND_PROCESSES},
                    MY_PERMISSIONS_REQUEST_LOCATION);

        }
    }

    public static void bringApplicationToFront(Activity activity) {

        MyLog.i("bring to front intent");

        Intent notificationIntent = new Intent(activity, RouteInfoActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, notificationIntent, 0);
        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    public static void killPackage(String packageName, Activity activity) {
        try {
            ActivityManager am = (ActivityManager) activity.getSystemService(Activity.ACTIVITY_SERVICE);
            checkPermission(activity);
            am.killBackgroundProcesses(packageName);
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    public static void amKillProcess(String process, Activity activity) {
        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();

        for (ActivityManager.RunningAppProcessInfo runningProcess : runningProcesses) {
            if (runningProcess.processName.equals(process)) {
                android.os.Process.sendSignal(runningProcess.pid, android.os.Process.SIGNAL_KILL);
            }
        }
    }
}

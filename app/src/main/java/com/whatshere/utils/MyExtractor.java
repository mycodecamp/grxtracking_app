package com.whatshere.utils;

import grx.tracking.core.persistence.OrderInfo;

public class MyExtractor {

    public static String extractOrderTypeToString(OrderInfo orderInfo){
        String type ="";
        if (orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK)) {
            type = "Retirada";
        }

        if (orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.DELIVERY)) {
            type = "Entrega";
        }

        if (orderInfo.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE)) {
            type = "Troca";
        }
        return type;
    }
}

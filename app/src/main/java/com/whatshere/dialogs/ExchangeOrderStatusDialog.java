package com.whatshere.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatshere.R;
import com.whatshere.http.HttpClient;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;

import java.util.ArrayList;
import java.util.List;

import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;

public class ExchangeOrderStatusDialog extends AlertDialog {

    private Callback callback;
    // private UserId userId;
    private Context context;

    public ExchangeOrderStatusDialog(Context context, final Callback callback) {
        super(context, R.style.MyAlertDialogStyleBlue);
        this.context = context;
        // this.userId = userId;
        this.callback = callback;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View content = LayoutInflater.from(getContext()).inflate(R.layout.select_list_layout, null);
        setView(content);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });


        setTitle("Troca de Caçamba");

        final RecyclerView rv = content.findViewById(R.id.rvOptions);

        LinearLayoutManager llm = new LinearLayoutManager(this.context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);
        rv.setAdapter(new SelectBaseAdapter(new ArrayList<SectorInfo>()));



        setButton(DialogInterface.BUTTON_POSITIVE, "Confirmar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SectorInfo base = ((SelectBaseAdapter) rv.getAdapter()).getSelected();
                        callback.processYes(base, true);
                    }
                });

        setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                        SectorInfo base = ((SelectBaseAdapter) rv.getAdapter()).getSelected();
                        callback.processYes(base,false);
                    }
                });

        super.onCreate(savedInstanceState);

        List<SectorInfo> bases = new ArrayList<>();
        {
            SectorInfo s1 = new SectorInfo();
            s1.setSectorId("split_to_deliver");
            s1.setName("Caçamba apenas Retirada");
            bases.add(s1);
        }

        {
            SectorInfo s1 = new SectorInfo();
            s1.setSectorId("split_to_pickup");
            s1.setName("Caçamba apenas Entregue");
            bases.add(s1);
        }

        {
            SectorInfo s1 = new SectorInfo();
            s1.setName("Caçamba Trocada");
            bases.add(s1);
        }


        rv.setAdapter(new SelectBaseAdapter(bases));

    }


    private class SelectBaseAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<SectorInfo> bases;
        private int selectedPosition = 2;


        public SelectBaseAdapter(List<SectorInfo> trucks) {
            this.bases = trucks;
            this.selectedColor = ((Activity) ExchangeOrderStatusDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) ExchangeOrderStatusDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) ExchangeOrderStatusDialog.this.context).getResources().getColor(R.color.black);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.base_item, parent, false);
            return new OptionViewHolder(itemView);
        }

        public SectorInfo getSelected() {
            if (selectedPosition != -1) {
                return bases.get(selectedPosition);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final SectorInfo base = this.bases.get(position);

            holder.itemView.setBackgroundColor(selectedPosition == position ? selectedColor : normalColor);

            ((OptionViewHolder) holder).img.setColorFilter(selectedPosition == position ? blackColor : selectedColor        );


            ((OptionViewHolder) holder).title.setText(base.getName());
            ((OptionViewHolder) holder).address.setText("");
            ((OptionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = position;
                    notifyItemChanged(selectedPosition);

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.bases.size();
        }
    }

    private class OptionViewHolder extends RecyclerView.ViewHolder {
        protected ImageView img;
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected View itemView;


        public OptionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    public static interface Callback {
        public void processYes(SectorInfo base, boolean sendToServer);


    }
}

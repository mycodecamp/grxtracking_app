package com.whatshere.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatshere.R;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.List;

import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.UserId;

public class OccurrenceDialog extends AlertDialog {

    private Callback callback;
    private UserId userId;
    private Context context;

    private RecyclerView rvOccurr;
    private RecyclerView rvOrders;

    private List<OrderInfo> orders;


    public OccurrenceDialog(Context context, UserId userId, List<OrderInfo> orders, final Callback callback) {
        super(context, R.style.MyAlertDialogStyleBlue);
        this.context = context;
        this.userId = userId;
        this.orders = orders;
        this.callback = callback;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View content = LayoutInflater.from(getContext()).inflate(R.layout.select_occurr_layout, null);
        setView(content);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });

        final EditText editDescr = content.findViewById(R.id.edit_description);


        setTitle("Selecionar Ocorrência");
        {
            this.rvOccurr = content.findViewById(R.id.rv_options);

            LinearLayoutManager llm = new LinearLayoutManager(this.context);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.rvOccurr.setLayoutManager(llm);


            List<OccurenceDescription> opts = new ArrayList<>();
            {
                OccurenceDescription o = new OccurenceDescription();
                o.setType(OccurrenceInfo.OCCURRENCE_TYPE.ORDER_PROBLEM);
                o.setDescription("");
                opts.add(o);
            }

            {
                OccurenceDescription o = new OccurenceDescription();
                o.setType(OccurrenceInfo.OCCURRENCE_TYPE.TRUCK_BREAK);
                o.setDescription("");
                opts.add(o);
            }
            this.rvOccurr.setAdapter(new SelectOccurrenceAdapter(opts));

        }

        {
            this.rvOrders = content.findViewById(R.id.rv_orders);

            LinearLayoutManager llm = new LinearLayoutManager(this.context);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.rvOrders.setLayoutManager(llm);

            this.rvOrders.setAdapter(new SelectOrderAdapter(this.orders));
        }
        setButton(DialogInterface.BUTTON_POSITIVE, "Confirmar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        OccurenceDescription occurr = ((SelectOccurrenceAdapter) rvOccurr.getAdapter()).getSelected();
                        OrderInfo order = ((SelectOrderAdapter) rvOrders.getAdapter()).getSelected();
                        String description = "";

                        try {
                            description = editDescr.getText().toString();
                        } catch (Exception e) {
                            MyLog.log(e);
                        }

                        callback.processYes(occurr, description, order);
                    }
                });

        setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                        callback.processNo();
                    }
                });

        super.onCreate(savedInstanceState);


    }

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    private class SelectOccurrenceAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<OccurenceDescription> occurrs;
        private int selectedPosition = -1;


        public SelectOccurrenceAdapter(List<OccurenceDescription> occurs) {
            this.occurrs = occurs;
            this.selectedColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.black);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.occurr_item, parent, false);
            return new OptionViewHolder(itemView);
        }

        public OccurenceDescription getSelected() {
            if (selectedPosition != -1) {
                return occurrs.get(selectedPosition);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final OccurenceDescription occurr = this.occurrs.get(position);


            holder.itemView.setBackgroundColor(selectedPosition == position ? selectedColor : normalColor);

            //((OptionViewHolder) holder).img.setColorFilter(selectedPosition == position ? blackColor : selectedColor);

            ((OptionViewHolder) holder).title.setText(occurr.getType().getName());
            ((OptionViewHolder) holder).address.setText(occurr.getDescription());
            ((OptionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = position;
                    notifyItemChanged(selectedPosition);

                    if (occurr.getType().equals(OccurrenceInfo.OCCURRENCE_TYPE.ORDER_PROBLEM)) {
                        rvOrders.setVisibility(View.VISIBLE);
                    } else {
                        rvOrders.setVisibility(View.GONE);
                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return this.occurrs.size();
        }
    }

    private class OptionViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected ImageView img;
        protected View itemView;


        public OptionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    private class SelectOrderAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<OrderInfo> orders;
        private int selectedPosition = -1;


        public SelectOrderAdapter(List<OrderInfo> orders) {
            this.orders = orders;
            this.selectedColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) OccurrenceDialog.this.context).getResources().getColor(R.color.black);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.occurr_item, parent, false);
            return new OrderViewHolder(itemView);
        }

        public OrderInfo getSelected() {
            if (selectedPosition != -1) {
                return orders.get(selectedPosition);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final OrderInfo order = this.orders.get(position);


            holder.itemView.setBackgroundColor(selectedPosition == position ? selectedColor : normalColor);

            // ((OrderViewHolder) holder).img.setColorFilter(selectedPosition == position ? blackColor : selectedColor);

            ((OrderViewHolder) holder).title.setText(order.getOrderId().getOrder_id());
            ((OrderViewHolder) holder).address.setText(order.getAddress().getAddress().asAddressString());
            ((OrderViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = position;
                    notifyItemChanged(selectedPosition);

                }
            });

            int order_icon = order.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK) ? R.drawable.marker_red_icon : R.drawable.marker_green_icon;
            ((OrderViewHolder) holder).img.setImageResource(order_icon);
        }

        @Override
        public int getItemCount() {
            return this.orders.size();
        }
    }

    private class OrderViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected ImageView img;
        protected View itemView;


        public OrderViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    public static interface Callback {
        public void processYes(OccurenceDescription occurr, String description, OrderInfo order);

        public void processNo();
    }

    public static class OccurenceDescription {
        private OccurrenceInfo.OCCURRENCE_TYPE type;
        private String description;

        public OccurrenceInfo.OCCURRENCE_TYPE getType() {
            return type;
        }

        public void setType(OccurrenceInfo.OCCURRENCE_TYPE type) {
            this.type = type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}


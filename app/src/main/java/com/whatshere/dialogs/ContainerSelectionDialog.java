package com.whatshere.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatshere.R;
import com.whatshere.http.HttpClient;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.utils.MyDialogs;

import java.util.ArrayList;
import java.util.List;

import grx.tracking.core.persistence.ContainerInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;

public class ContainerSelectionDialog extends AlertDialog {

    private Callback callback;

    private Activity context;

    private SectorInfo sector;

    private RecyclerView rv;

    public ContainerSelectionDialog(Activity context, SectorInfo sector, final Callback callback) {
        super(context, R.style.MyAlertDialogStyleBlue);
        this.context = context;
        this.callback = callback;
        this.sector = sector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View content = LayoutInflater.from(getContext()).inflate(R.layout.select_list_layout, null);
        setView(content);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });


        setTitle("Caçambas");

        this.rv = content.findViewById(R.id.rvOptions);

        LinearLayoutManager llm = new LinearLayoutManager(this.context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.rv.setLayoutManager(llm);
        this.rv.setAdapter(new SelectContainerAdapter(new ArrayList<ContainerInfo>()));

        ExecuteTask.executeTask(new TaskDelegator() {

            private List<ContainerInfo> containers = new ArrayList<>();

            @Override
            public void doInBackground() throws Exception {
                this.containers = HttpClient.getRef().getContainers(sector.getSectorId());
            }

            @Override
            public void onPostExecute() {
                rv.setAdapter(new SelectContainerAdapter(containers));
            }
        });


        setButton(DialogInterface.BUTTON_POSITIVE, "Confirmar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });


       /* setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });
                */

        super.onCreate(savedInstanceState);


    }

    @Override
    public void show() {
        super.show();
        this.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<ContainerInfo> containers = ((ContainerSelectionDialog.SelectContainerAdapter) rv.getAdapter()).getSelected();
                callback.processYes(containers);

                if (containers.isEmpty()) {
                    MyDialogs.showError(context, "Caçambas", "Selecione uma caçamba");
                } else {
                    dismiss();
                }
            }
        });
    }

    private class SelectContainerAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<ContainerInfo> containers;
        private List<ContainerInfo> selected;


        public SelectContainerAdapter(List<ContainerInfo> containers) {
            this.containers = containers;
            this.selected = new ArrayList<>();
            this.selectedColor = ((Activity) ContainerSelectionDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) ContainerSelectionDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) ContainerSelectionDialog.this.context).getResources().getColor(R.color.black);
        }

        public List<ContainerInfo> getSelected() {
            return selected;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.base_item, parent, false);
            return new OptionViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final ContainerInfo container = this.containers.get(position);

            holder.itemView.setBackgroundColor(selected.contains(container) ? selectedColor : normalColor);

            ((OptionViewHolder) holder).img.setColorFilter(selected.contains(container) ? blackColor : selectedColor);


            ((OptionViewHolder) holder).title.setText(container.getCac_codigocacamba());
            ((OptionViewHolder) holder).address.setText("");
            ((OptionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(position);

                    if (selected.contains(container)) {
                        selected.remove(container);
                    } else {
                        selected.add(container);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.containers.size();
        }
    }

    private class OptionViewHolder extends RecyclerView.ViewHolder {
        protected ImageView img;
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected View itemView;


        public OptionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    public static interface Callback {
        public void processYes(List<ContainerInfo> containers);

        public void processNo();
    }
}

package com.whatshere.dialogs;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatshere.R;
import com.whatshere.RouteInfoActivity;
import com.whatshere.SurfaceCameraActivity;
import com.whatshere.http.HttpClient;
import com.whatshere.persistence.DAO;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import grx.tracking.core.persistence.ContainerInfo;
import grx.tracking.core.persistence.UserId;

public class CheckoutContainerSelectionDialog extends AlertDialog {

    private Callback callback;

    private Activity context;


    private DAO dao;

    public CheckoutContainerSelectionDialog(Activity context, DAO dao, final Callback callback) {
        super(context, R.style.MyAlertDialogStyleBlue);
        this.context = context;
        this.callback = callback;
        this.dao = dao;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View content = LayoutInflater.from(getContext()).inflate(R.layout.select_list_layout_check, null);
        setView(content);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });


        setTitle("Caçamba Entregue");

        final RecyclerView rv = content.findViewById(R.id.rvOptions);
        final ImageButton take_picture = content.findViewById(R.id.btn_take_picture);
        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(context, new String[]{android.Manifest.permission.CAMERA
                    }, 50);
                } else {
                    Intent i = new Intent(context, SurfaceCameraActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                }
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this.context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);


        this.dao.retrieveObject("containers", String.class, new DAO.Callback<String>() {
            @Override
            public void retrieve(String obj) {

                try {
                    JSONConverter conv = new JSONConverter();
                    ContainerInfo[] arrays = conv.toObject(obj, ContainerInfo[].class);
                    rv.setAdapter(new SelectContainerAdapter(new ArrayList<>(Arrays.asList(arrays))));
                    rv.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    MyLog.log(e);
                    rv.setAdapter(new SelectContainerAdapter(new ArrayList<>()));
                }
            }
        });


        setButton(DialogInterface.BUTTON_POSITIVE, "Confirmar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        List<ContainerInfo> containers = ((SelectContainerAdapter) rv.getAdapter()).getSelected();
                        callback.processYes(containers);
                    }
                });


        super.onCreate(savedInstanceState);


    }


    private class SelectContainerAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<ContainerInfo> containers;
        private int selectedPosition = -1;


        public SelectContainerAdapter(List<ContainerInfo> containers) {
            this.containers = containers;
            this.selectedColor = ((Activity) CheckoutContainerSelectionDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) CheckoutContainerSelectionDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) CheckoutContainerSelectionDialog.this.context).getResources().getColor(R.color.black);
        }

        public List<ContainerInfo> getSelected() {
            return containers;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.base_item, parent, false);
            return new OptionViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final ContainerInfo container = this.containers.get(position);

            holder.itemView.setBackgroundColor(selectedPosition == position ? selectedColor : normalColor);

            ((OptionViewHolder) holder).img.setColorFilter(selectedPosition == position ? blackColor : selectedColor);


            ((OptionViewHolder) holder).title.setText(container.getCac_codigocacamba());
            ((OptionViewHolder) holder).address.setText("");
            ((OptionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = position;
                    notifyItemChanged(selectedPosition);

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.containers.size();
        }
    }

    private class OptionViewHolder extends RecyclerView.ViewHolder {
        protected ImageView img;
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected View itemView;


        public OptionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    public static interface Callback {
        public void processYes(List<ContainerInfo> containers);

    }
}

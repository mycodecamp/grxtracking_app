package com.whatshere.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatshere.R;
import com.whatshere.http.HttpClient;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.ui.MapViewer;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.List;

import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;

public class TruckSelectionDialog extends AlertDialog {

    private Callback callback;
    private UserId userId;
    private Context context;

    public TruckSelectionDialog(Context context, UserId userId, final Callback callback) {
        super(context, R.style.MyAlertDialogStyleBlue);
        this.context = context;
        this.userId = userId;
        this.callback = callback;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View content = LayoutInflater.from(getContext()).inflate(R.layout.select_list_layout, null);
        setView(content);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });


        setTitle("Selecionar Caminhão");

        final RecyclerView rv = content.findViewById(R.id.rvOptions);

        LinearLayoutManager llm = new LinearLayoutManager(this.context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);
        rv.setAdapter(new SelectTruckAdapter(new ArrayList<TruckInfo>()));

        ExecuteTask.executeTask(new TaskDelegator() {

            private List<TruckInfo> trucks = new ArrayList<>();

            @Override
            public void doInBackground() throws Exception {
                this.trucks = HttpClient.getRef().getTrucks(userId);
            }

            @Override
            public void onPostExecute() {
                rv.setAdapter(new SelectTruckAdapter(trucks));
            }
        });

        setButton(DialogInterface.BUTTON_POSITIVE, "Confirmar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TruckInfo truck = ((SelectTruckAdapter) rv.getAdapter()).getSelected();
                        callback.processYes(truck);
                    }
                });

        setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });

        super.onCreate(savedInstanceState);


    }


    private class SelectTruckAdapter extends RecyclerView.Adapter {

        private int blackColor;
        private int normalColor;
        private int selectedColor;
        private List<TruckInfo> trucks;
        private int selectedPosition = -1;


        public SelectTruckAdapter(List<TruckInfo> trucks) {
            this.trucks = trucks;
            this.selectedColor = ((Activity) TruckSelectionDialog.this.context).getResources().getColor(R.color.main_select_theme);
            this.normalColor = ((Activity) TruckSelectionDialog.this.context).getResources().getColor(R.color.main_color_theme);
            this.blackColor = ((Activity) TruckSelectionDialog.this.context).getResources().getColor(R.color.black);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.truck_item, parent, false);
            return new OptionViewHolder(itemView);
        }

        public TruckInfo getSelected() {
            if (selectedPosition != -1) {
                return trucks.get(selectedPosition);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final TruckInfo truck = this.trucks.get(position);

            MyLog.i("placa: " + truck.getPlaca());


            holder.itemView.setBackgroundColor(selectedPosition == position ? selectedColor : normalColor);

            ((OptionViewHolder) holder).img.setColorFilter(selectedPosition == position ? blackColor : selectedColor);

            ((OptionViewHolder) holder).title.setText(truck.getPlaca());
            ((OptionViewHolder) holder).address.setText(truck.getTruckDescription());
            ((OptionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = position;
                    notifyItemChanged(selectedPosition);

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.trucks.size();
        }
    }

    private class OptionViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected TextView type;
        protected TextView address;
        protected ImageView img;
        protected View itemView;


        public OptionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = (TextView) itemView.findViewById(R.id.lblType);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }


    }


    public static interface Callback {
        public void processYes(TruckInfo truck);

        public void processNo();
    }
}

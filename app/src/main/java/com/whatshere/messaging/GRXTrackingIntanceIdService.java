package com.whatshere.messaging;

import com.google.common.eventbus.EventBus;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.MyLog;

import grx.tracking.core.transport.TokenInfo;

public class GRXTrackingIntanceIdService extends FirebaseInstanceIdService {

    private EventBus eventBus;

    public GRXTrackingIntanceIdService() {
        this.eventBus = EventBusIntance.getEventBus();
    }

    @Override
    public void onTokenRefresh() {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        MyLog.i("refresh token: " + refreshedToken);

        TokenInfo token = new TokenInfo();
        token.setTokenInfo(refreshedToken);

        this.eventBus.post(token);

    }
}

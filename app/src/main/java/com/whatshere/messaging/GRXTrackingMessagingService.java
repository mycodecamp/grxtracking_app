package com.whatshere.messaging;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.whatshere.Login;
import com.whatshere.MainActivity;
import com.whatshere.RouteInfoActivity;
import com.whatshere.SplashScreen;
import com.whatshere.eventbus.CancelOrderEvent;
import com.whatshere.eventbus.CancelRouteEvent;
import com.whatshere.eventbus.ErrorRouteEvent;
import com.whatshere.eventbus.NotificationEvent;
import com.whatshere.eventbus.RouteInfoReceivedEvent;
import com.whatshere.eventbus.WaitingRouteEvent;
import com.whatshere.http.HttpClient;
import com.whatshere.persistence.DAO;
import com.whatshere.persistence.RouteInfoWrapper;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyLog;
import com.whatshere.eventbus.OrdersReceivedEvent;
import com.whatshere.utils.PackagesUtils;

import java.util.List;

import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.CancelResultMessage;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.RouteResultMessage;
import grx.tracking.core.transport.TokenInfo;

public class GRXTrackingMessagingService extends FirebaseMessagingService {

    private DAO dao = new DAO();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        MyLog.i("onMessageReceived: " + remoteMessage);

        {
            try {
                String androidId = android.provider.Settings.Secure.getString(getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);
                DAO.AndroidID = androidId;
                MyLog.i("### AndroidID: " + DAO.AndroidID + " ###");
            } catch (Throwable t) {
                MyLog.log(t);
            }
        }

        MyLog.i("onMessageReceived: " + remoteMessage.getData());

        try {
            String jsonGoogle = remoteMessage.getData().get("jsonString");
            String eventClass = remoteMessage.getData().get("eventClass");

            JSONConverter conv = new JSONConverter();

            Object content = conv.toObject(jsonGoogle, Class.forName(eventClass));

            MyLog.i("%%%%%% content object: " + content + " %%%%%%%");

            if (content instanceof CancelResultMessage) {
                final CancelResultMessage cancel_result = (CancelResultMessage) content;
                if (cancel_result.getWasCancel().equals("route")) {
                    String route_id = cancel_result.getRoute_id();

                    CancelRouteEvent event = new CancelRouteEvent();
                    event.route_id = route_id;

                    EventBusIntance.getEventBus().post(event);
                }

                if (cancel_result.getWasCancel().equals("order")) {
                    OrderId orderId = cancel_result.getOrderId();


                    CancelOrderEvent event = new CancelOrderEvent();
                    event.order_id = orderId;

                    EventBusIntance.getEventBus().post(event);
                }

            }

            if (content instanceof RouteResultMessage) {

                final RouteResultMessage route_result = (RouteResultMessage) content;

                if (route_result.getStatus().equals("OK") || route_result.getStatus().equals("OK_FOR_WAITING")) {
                    MyLog.i("trying GRXTracking");

                    boolean running = PackagesUtils.isAppRunning(GRXTrackingMessagingService.this, "br.com.rafaentulhos.locationapp");

                    MyLog.i("running: " + running);

                    if (route_result.getStatus().equals("OK_FOR_WAITING") &&
                            !running) {


                        this.dao.retrieveObject("last_token", String.class, new DAO.Callback<String>() {
                            @Override
                            public void retrieve(String last_token) {

                                HttpClient.getRef().setToken(last_token);

                                ExecuteTask.executeTask(new TaskDelegator() {
                                    @Override
                                    public void doInBackground() throws Exception {
                                        RouteInfo route = HttpClient.getRef().getRoute(route_result.getRoute().getId() + "");
                                        RouteInfoReceivedEvent event = new RouteInfoReceivedEvent();
                                        event.route = route;


                                        JSONConverter conv = new JSONConverter();
                                        String asJson = conv.toJson(RouteInfoWrapper.wrap(route));

                                        dao.saveObj("current_route_as_json", asJson);
                                    }

                                    @Override
                                    public void onPostExecute() {
                                        if (taskException != null) {
                                            MyLog.log(taskException);
                                        }
                                    }
                                });

                                ExecuteTask.executeTask(new TaskDelegator() {
                                    @Override
                                    public void doInBackground() throws Exception {
                                        List<OrderInfo> orders = HttpClient.getRef().getRouteOrders(route_result.getRoute().getId() + "");
                                        OrdersReceivedEvent event = new OrdersReceivedEvent();
                                        event.orders = orders;
                                        // EventBusIntance.getEventBus().post(event);
                                        dao.saveObj("current_orders", orders);
                                    }

                                    @Override
                                    public void onPostExecute() {
                                        if (taskException != null) {
                                            MyLog.log(taskException);
                                        }

                                        Intent intent = new Intent(GRXTrackingMessagingService.this, RouteInfoActivity.class);
                                        Bundle extras = new Bundle();
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        //  intent.putExtras(extras);
                                        startActivity(intent);
                                    }
                                });


                            }


                        });
                    } else {

                        ExecuteTask.executeTask(new TaskDelegator() {
                            @Override
                            public void doInBackground() throws Exception {
                                RouteInfo route = HttpClient.getRef().getRoute(route_result.getRoute().getId() + "");
                                RouteInfoReceivedEvent event = new RouteInfoReceivedEvent();
                                event.route = route;
                                EventBusIntance.getEventBus().post(event);
                            }

                            @Override
                            public void onPostExecute() {
                                if (taskException != null) {
                                    MyLog.log(taskException);
                                }
                            }
                        });

                        ExecuteTask.executeTask(new TaskDelegator() {
                            @Override
                            public void doInBackground() throws Exception {
                                List<OrderInfo> orders = HttpClient.getRef().getRouteOrders(route_result.getRoute().getId() + "");
                                OrdersReceivedEvent event = new OrdersReceivedEvent();
                                event.orders = orders;
                                EventBusIntance.getEventBus().post(event);
                            }

                            @Override
                            public void onPostExecute() {
                                if (taskException != null) {
                                    MyLog.log(taskException);
                                }
                            }
                        });
                    }

                    if (route_result.getStatus().equals("OK_FOR_WAITING")) {
                        WaitingRouteEvent event = new WaitingRouteEvent();
                        event.message = route_result;
                        event.status = "ok_waiting";
                        EventBusIntance.getEventBus().post(event);
                    }
                }

                if (route_result.getStatus().equals("WAITING_CONFIRM")) {
                    WaitingRouteEvent event = new WaitingRouteEvent();
                    event.message = route_result;
                    event.status = "waiting";
                    EventBusIntance.getEventBus().post(event);
                }

                if (route_result.getStatus().startsWith("TIMEOUT")) {
                    ErrorRouteEvent error = new ErrorRouteEvent();
                    route_result.setMessage("Tempo de espera para calculo de rotas excedido no servidor");
                    error.messageError = route_result;
                    EventBusIntance.getEventBus().post(error);
                }

                if (route_result.getStatus().startsWith("ERROR")) {
                    ErrorRouteEvent error = new ErrorRouteEvent();
                    error.messageError = route_result;
                    EventBusIntance.getEventBus().post(error);
                }

                if (route_result.getStatus().equals("SEM ROTAS")) {
                    ErrorRouteEvent error = new ErrorRouteEvent();
                    route_result.setMessage("Nenhum pedido encontrado");
                    error.messageError = route_result;
                    EventBusIntance.getEventBus().post(error);
                }
            }


        } catch (Exception e) {
            MyLog.log(e);
        }

        Runtime.getRuntime().gc();
    }
}

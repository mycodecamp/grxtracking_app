package com.whatshere;

import android.Manifest;
import android.arch.core.executor.TaskExecutor;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.maps.model.LatLng;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.whatshere.dialogs.BaseSelectionDialog;
import com.whatshere.dialogs.CheckoutContainerSelectionDialog;
import com.whatshere.dialogs.ContainerSelectionDialog;
import com.whatshere.dialogs.ExchangeOrderStatusDialog;
import com.whatshere.dialogs.OccurrenceDialog;
import com.whatshere.dialogs.QtdeContainerDialog;
import com.whatshere.dialogs.TruckSelectionDialog;
import com.whatshere.eventbus.CancelOrderEvent;
import com.whatshere.eventbus.CancelRouteEvent;
import com.whatshere.eventbus.ErrorRouteEvent;
import com.whatshere.eventbus.LocationEvent;
import com.whatshere.eventbus.NotificationEvent;
import com.whatshere.eventbus.ProximityEvent;
import com.whatshere.eventbus.RemoveOrderEvent;
import com.whatshere.eventbus.RouteInfoReceivedEvent;
import com.whatshere.eventbus.WaitingRouteEvent;
import com.whatshere.http.HttpClient;
import com.whatshere.location.LocationUpdater;
import com.whatshere.location.RouteCalculator;
import com.whatshere.model.Order;
import com.whatshere.persistence.DAO;
import com.whatshere.persistence.DAO.Callback;
import com.whatshere.persistence.RouteInfoWrapper;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.ui.MapViewer;
import com.whatshere.ui.RoutePoints;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.GRXExceptionHandler;
import com.whatshere.utils.GeolocationsEvent;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyLog;
import com.whatshere.eventbus.OrdersReceivedEvent;
import com.whatshere.utils.PackagesUtils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import dmax.dialog.SpotsDialog;
// import grx.tracking.core.events.EventCancelOrder;
// import grx.tracking.core.events.EventCancelRoute;
import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.persistence.ContainerInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.RouteStatInfo;
import grx.tracking.core.persistence.RouteStatus;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.core.transport.RouteResultMessage;

/**
 * The RouteInfoActivity is the activity class that shows Map fragment and List
 * Fragment. This activity shows RoutePoints listing with two option to view places
 * on Map or in List. You need to write actual logic for displaying real place
 * listing and locations. Both List and Map are added in ViewPager to provide
 * Sliding navigation.
 */
public class RouteInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private SlidingUpPanelLayout slideLayout;
    private RelativeLayout dragViewPrice;
    /**
     * The search view.
     */
    private SearchView searchView;

    /**
     * The current tab.
     */
    private View currentTab;

    /**
     * The pager.
     */
    private ViewPager pager;

    /**
     * event bus *
     */
    private EventBus eventBus;

    /**
     * rota encontrada
     */
    private RouteInfoWrapper routeInfo;
    private List<OrderInfo> ordersInfo;
    private UserInfo userInfo;
    private TruckId truckId;

    /**
     * dao
     */
    private DAO dao;

    /**
     * map view
     */
    private MapViewer mapViewer;

    /**
     * views
     */
    private RoutePoints placeViewer;
    private FloatingActionMenu menuFab;

    /**
     * fused client
     */
    private FusedLocationProviderClient fusedProvider;
    private SensorEventListener sensorListener;
    private RouteResultMessage errorResult;
    private RouteResultMessage watingMessage;
    /**
     * route manager
     */
    private RouteCalculator routeCalculator;

    private TextView textViewOrder;
    private TextView textViewAddress;
    private Button btnNavigate;


    /* (non-Javadoc)
     * @see com.food.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.route_info);

        this.textViewOrder = this.findViewById(R.id.textview_order_info);
        this.textViewAddress = this.findViewById(R.id.textview_address);
        this.btnNavigate = this.findViewById(R.id.btn_navigate);
        this.slideLayout = this.findViewById(R.id.sliding_layout);
        this.dragViewPrice = this.findViewById(R.id.dragViewPrice);


        this.eventBus = EventBusIntance.getEventBus();
        this.eventBus.register(LocationUpdater.getRef());

        try {
            this.eventBus.unregister(this);
        } catch (Throwable t) {
            MyLog.i("controlled exception: " + t.getLocalizedMessage());
        }
        this.eventBus.register(this);

        this.dao = new DAO();

        initTabs();
        initPager();

        // getActionBar().setTitle(getIntent().getStringExtra("title"));
        // getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_theme)));

        {
            // just for precaution
            if (DAO.AndroidID == null) {
                String androidId = android.provider.Settings.Secure.getString(getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);
                DAO.AndroidID = androidId;
                MyLog.i("### AndroidID: " + DAO.AndroidID + " ###");
            }
        }
        {
            // retrieve user
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                MyLog.i("retrieve user from intent");
                this.userInfo = (UserInfo) extras.getSerializable("userinfo");
            } else {
                this.dao.retrieveObject("userinfo", UserInfo.class, new Callback<UserInfo>() {
                    @Override
                    public void retrieve(UserInfo userInfo) {
                        MyLog.i("retrieve user from firebase");
                        RouteInfoActivity.this.userInfo = userInfo;
                    }
                });
            }
        }

        // fused location
        {
            this.checkLocationPermission();

            this.fusedProvider = LocationServices.getFusedLocationProviderClient(this);

            LocationRequest locationUpdates = new LocationRequest();
            locationUpdates.setInterval(5000);
            locationUpdates.setFastestInterval(5000);
            locationUpdates.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            this.fusedProvider.requestLocationUpdates(locationUpdates, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    if (RouteInfoActivity.this.userInfo != null) {
                        LocationEvent event = new LocationEvent();
                        event.result = locationResult;
                        event.userId = userInfo.getUserId();
                        eventBus.post(event);
                    }
                }
            }, null);

            this.requestLastLocation();
        }

        // sensors
        {
            SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);

            this.sensorListener = new SensorEventListener();
            boolean hasS1 = sm.registerListener(this.sensorListener, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
            boolean hasS2 = sm.registerListener(this.sensorListener, sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                    SensorManager.SENSOR_DELAY_NORMAL);
            boolean hasS3 = sm.registerListener(this.sensorListener, sm.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                    SensorManager.SENSOR_DELAY_NORMAL);
            MyLog.i("hasS3: " + hasS3);
        }

        // fab menu
        {
            this.menuFab = this.findViewById(R.id.fabmenu);

            FloatingActionButton fabcancel = this.findViewById(R.id.fab_cancel_route);
            fabcancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MyDialogs.showConfirm(RouteInfoActivity.this, "Rota", "Deseja cancelar a rota?", new MyDialogs.Callback() {
                        @Override
                        public void processYes() {
                            cancelRoute(true, true);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    menuFab.close(true);
                                }
                            });
                        }

                        @Override
                        public void processNo() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    menuFab.close(true);
                                }
                            });
                        }
                    });
                }

            });


            FloatingActionButton faboccurr = this.findViewById(R.id.fab_occurrence);
            faboccurr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OccurrenceDialog dialog = new OccurrenceDialog(RouteInfoActivity.this, userInfo.getUserId(), ordersInfo, new OccurrenceDialog.Callback() {
                        @Override
                        public void processYes(final OccurrenceDialog.OccurenceDescription occurr, final String description, final OrderInfo order) {
                            ExecuteTask.executeTask(new TaskDelegator() {
                                @Override
                                public void doInBackground() throws Exception {
                                    JSONConverter conv = new JSONConverter();

                                    EventOcurrence event = new EventOcurrence();
                                    event.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                                    event.setTo(GRXEvent.TO_TYPE.TO_USER);
                                    event.setType(GRXEvent.EVENT_TYPE.REAL_TIME);
                                    event.setDateEvent(Calendar.getInstance().getTime());
                                    OccurrenceInfo ocurrence = new OccurrenceInfo();
                                    ocurrence.setType(occurr.getType());
                                    ocurrence.setDescription(description);
                                    ocurrence.setUserId(RouteInfoActivity.this.userInfo.getUserId());
                                    ocurrence.setTruckId(RouteInfoActivity.this.truckId);
                                    ocurrence.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());

                                    if (order != null) {
                                        ocurrence.setOrderId(order.getOrderId());
                                    }

                                    ocurrence.setDate(Calendar.getInstance().getTime());
                                    event.setOcurrence(ocurrence);

                                    EventMessage message = new EventMessage();
                                    message.setEventClass(EventOcurrence.class.getCanonicalName());
                                    message.setJsonString(conv.toJson(event));

                                    HttpClient.getRef().sendEvent(message);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            menuFab.close(true);
                                        }
                                    });
                                }

                                @Override
                                public void onPostExecute() {
                                    if (super.taskException == null) {
                                        MyDialogs.showInformation(RouteInfoActivity.this, "Informar Ocorrência", "Ocorrência informada com sucesso", new MyDialogs.MyDialogListener() {
                                            @Override
                                            public void processClose() {

                                            }
                                        });
                                    } else {
                                        MyLog.log(super.taskException);
                                        MyDialogs.showError(RouteInfoActivity.this, "Informar Ocorrência", "Erro: " + super.taskException.getLocalizedMessage());

                                    }
                                }
                            });
                        }

                        @Override
                        public void processNo() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    menuFab.close(true);
                                }
                            });
                        }
                    });
                    dialog.show();
                }
            });

            FloatingActionButton fabcancel_order = this.findViewById(R.id.fab_cancel_order);
            fabcancel_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final OrderInfo currentOrder = routeCalculator.getCurrentOrder();
                    MyDialogs.showConfirm(RouteInfoActivity.this, "Cancelar Pedido", "Deseja cancelar o pedido: " + currentOrder.getOrderId() + "?", new MyDialogs.Callback() {
                                @Override

                                public void processYes() {
                                    try {

                                        RemoveOrderEvent event1 = new RemoveOrderEvent();
                                        event1.order = currentOrder;
                                        event1.notifyToServer = true;

                                        processRemoveOrder(event1);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                menuFab.close(true);
                                            }
                                        });

                                    } catch (Exception e) {
                                        MyLog.log(e);
                                        MyDialogs.showError(RouteInfoActivity.this, "Cancelar Pedido", "Erro: " + e.getLocalizedMessage());
                                    }
                                }

                                @Override
                                public void processNo() {

                                }
                            }
                    );


                }
            });

            FloatingActionButton fab_checkin_order = this.findViewById(R.id.fab_checkin_order);
            fab_checkin_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final OrderInfo currentOrder = routeCalculator.getCurrentOrder();
                    MyDialogs.showConfirm(RouteInfoActivity.this, "Check-in Pedido", "Deseja finalizar o pedido: " + currentOrder.getOrderId().getOrder_id() + "?", new MyDialogs.Callback() {

                                @Override
                                public void processYes() {
                                    ProximityEvent event = new ProximityEvent();
                                    event.order = currentOrder;
                                    receiveProximity(event);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            menuFab.close(true);
                                        }
                                    });
                                }

                                @Override
                                public void processNo() {

                                }
                            }
                    );
                }
            });



            FloatingActionButton fab_picture = this.findViewById(R.id.fab_picture);
            fab_picture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (ContextCompat.checkSelfPermission(RouteInfoActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RouteInfoActivity.this, new String[]{android.Manifest.permission.CAMERA
                        }, 50);
                    } else {
                        Intent i = new Intent(RouteInfoActivity.this, SurfaceCameraActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(i);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            menuFab.close(true);
                        }
                    });


                }
            });

            this.menuFab.setOnMenuButtonClickListener(new MenuFabClickListener());

        }
        {
            btnNavigate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OrderInfo order = routeCalculator.getCurrentOrder();
                    GeoLocation location = order.getAddress().getLocation();

                    try {
                        if (PackagesUtils.isPackageInstalled("com.waze", RouteInfoActivity.this)) {
                            String url = "https://waze.com/ul?ll=" + location.getLatitude() + "," + location.getLongitude() + "&navigate=yes";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);

                            return;
                        }
                    } catch (Exception e) {
                        MyLog.log(e);
                    }

                    try {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + location.getLatitude() + "," + location.getLongitude() + "&mode=d");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    } catch (Exception e) {
                        MyLog.log(e);
                    }
                    menuFab.close(true);
                }
            });
        }

        // load data
        {
            if (this.routeInfo == null && this.ordersInfo == null) {
                this.retrieveOldRouteInfo();
            }
        }
        {
            this.slideLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
                @Override
                public void onPanelSlide(View panel, float slideOffset) {

                }

                @Override
                public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                    try {
                        final TextView value = dragViewPrice.findViewById(R.id.ride_value);
                        if (SlidingUpPanelLayout.PanelState.EXPANDED.equals(newState)) {
                            ExecuteTask.executeTask(new TaskDelegator() {
                                RouteStatInfo payment;

                                @Override
                                public void doInBackground() throws Exception {
                                    this.payment = HttpClient.getRef().getPayment(routeInfo.getRoute().getId());

                                }

                                @Override
                                public void onPostExecute() {
                                    if (this.payment != null) {
                                        value.setText("R$ " + payment.getTotal_value());
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        MyLog.log(e);
                    }
                }
            });
        }


        MyLog.i("########################## onCreate #########################");

    }


    public RouteInfoWrapper getRouteInfo() {
        return routeInfo;
    }

    public List<OrderInfo> getOrdersInfo() {
        return ordersInfo;
    }

    public RouteResultMessage getErrorResult() {
        return errorResult;
    }

    public RouteResultMessage getWatingMessage() {
        return watingMessage;
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////


    // cancel order event
    // event from server, it call main function for cancel order
    @Subscribe
    public void processCancelOrder(CancelOrderEvent event) {
        if (event == null || event.order_id == null) {
            return;
        }

        final OrderId orderId = event.order_id;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    MyDialogs.showInformation(RouteInfoActivity.this, "Pedido Cancelado", "O pedido " + orderId.getOrder_id() + " foi cancelado pelo operador.", new MyDialogs.MyDialogListener() {
                        @Override
                        public void processClose() {

                        }
                    });

                    if (ordersInfo != null) {
                        OrderInfo toCancel = null;

                        for (OrderInfo order : ordersInfo) {
                            if (order.getOrderId().equals(orderId)) {
                                toCancel = order;
                                break;
                            }
                        }

                        if (toCancel != null) {
                            RemoveOrderEvent event1 = new RemoveOrderEvent();
                            event1.order = toCancel;
                            event1.notifyToServer = false;
                            processRemoveOrder(event1);
                        }
                    }
                } catch (Exception e) {
                    MyLog.log(e);
                    MyDialogs.showError(RouteInfoActivity.this, "Cancelar Pedido", "Erro: " + e.getLocalizedMessage());

                }
            }
        });

    }


    // cancel route event from server
    @Subscribe
    public void processCancelRoute(CancelRouteEvent event) {
        if (event == null || event.route_id == null) {
            return;
        }

        final String route_id = event.route_id;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    MyDialogs.showInformation(RouteInfoActivity.this, "Rota Cancelada", "A rota atual foi cancelada pelo operador.", new MyDialogs.MyDialogListener() {
                        @Override
                        public void processClose() {

                        }
                    });

                    // se a rota corrente eh a mesma
                    if (RouteInfoActivity.this.routeInfo != null && RouteInfoActivity.this.routeInfo.getRoute().getId().equals(route_id)) {
                        cancelRoute(false, true);
                    }
                } catch (Exception e) {
                    MyLog.log(e);
                    MyDialogs.showError(RouteInfoActivity.this, "Rota Cancelada", "Erro: " + e.getLocalizedMessage());

                }
            }
        });


    }

    // notification error
    @Subscribe
    public void processNotification(final NotificationEvent event) {
        try {
            MyDialogs.showInformation(this, "Rota Encontrada", event.message, new MyDialogs.MyDialogListener() {
                @Override
                public void processClose() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textViewOrder.setText(event.message);
                        }
                    });
                }
            });


        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    // receive route response
    @Subscribe
    public void processRouteResponse(RouteInfoReceivedEvent event) {
        try {
            if (event.route == null) {
                MyLog.i("##### route empty ######");
                return;
            }

            MyLog.i("RouteInfoReceivedEvent route: " + event.route + " points size: " + (event.route.getPoints() == null ? "sem points" : event.route.getPoints().length));
            if (event.route == null || event.route.getPoints() == null) {
                MyLog.i("event error: " + event);
                return;
            }

            this.routeInfo = RouteInfoWrapper.wrap(event.route);

            JSONConverter conv = new JSONConverter();
            String asJson = conv.toJson(this.routeInfo);

            this.dao.saveObj("current_route_as_json", asJson);
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    // receive orders info
    @Subscribe
    synchronized public void updateOrders(OrdersReceivedEvent event) {
        if (event.orders == null) {
            MyLog.i("##### orders empty ######");
            return;
        }

        MyLog.i("OrdersReceivedEvent orders: " + event.orders);
        this.ordersInfo = new ArrayList<>(event.orders);
        this.dao.saveObj("current_orders", this.ordersInfo);

        MyLog.i("this.routeCalculator: " + this.routeCalculator);
        // initialize route route calculator
        if (this.routeCalculator == null) {
            this.routeCalculator = new RouteCalculator(this.ordersInfo);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int nn = 0;
                    for (OrderInfo order : ordersInfo) {
                        if (OrderInfo.ORDER_TYPE.DELIVERY.equals(order.getOrderType()) || OrderInfo.ORDER_TYPE.EXCHANGE.equals(order.getOrderType())) {
                            nn++;
                        }
                    }

                    MyDialogs.showInformation(RouteInfoActivity.this, "Rota Encontrada", "Carregar " + nn + " caçambas para Entrega", new MyDialogs.MyDialogListener() {
                        @Override
                        public void processClose() {
                            showContainers();
                        }
                    });


                }
            });
        }

        MyLog.i("this.routeCalculator after: " + this.routeCalculator);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                menuFab.close(true);
            }
        });
    }

    // remove route
    private void removeLocalRoute() {
        this.dao.removeObject("current_route");
        this.dao.removeObject("current_route_as_json");
        this.dao.removeObject("current_orders");
        this.routeCalculator = null;
        this.mapViewer.cleanRouteInfo();
        this.placeViewer.clearOrderList();
        this.routeInfo = null;
        this.ordersInfo = null;
        this.errorResult = null;

        this.textViewOrder.setText("");
        this.textViewAddress.setText("");
        this.btnNavigate.setVisibility(View.INVISIBLE);


        MyLog.i("@@@ invisible 2 @@@");
        this.dragViewPrice.setVisibility(View.INVISIBLE);


        Runtime.getRuntime().gc();

        requestLastLocation();
    }

    private void cancelRoute(boolean notifyServer, final boolean cancel1) {
        try {
            if (notifyServer) {
                ExecuteTask.executeTask(new TaskDelegator() {
                    @Override
                    public void doInBackground() throws Exception {
                        JSONConverter conv = new JSONConverter();

                        EventChangeStatusRoute cancel = new EventChangeStatusRoute();
                        cancel.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());
                        cancel.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                        cancel.setTo(GRXEvent.TO_TYPE.TO_USER);
                        cancel.setType(GRXEvent.EVENT_TYPE.REAL_TIME);
                        RouteStatus newStatus = new RouteStatus();
                        newStatus.setStatus((cancel1 ? RouteStatus.ROUTE_STATUS.CANCEL : RouteStatus.ROUTE_STATUS.COMPLETE));
                        newStatus.setTimeDate(Calendar.getInstance().getTime());
                        cancel.setNewStatus(newStatus);

                        EventMessage message = new EventMessage();
                        message.setEventClass(EventChangeStatusRoute.class.getCanonicalName());
                        message.setJsonString(conv.toJson(cancel));

                        HttpClient.getRef().sendEvent(message);
                    }

                    @Override
                    public void onPostExecute() {
                        if (super.taskException == null) {
                            removeLocalRoute();
                        } else {
                            MyLog.log(super.taskException);
                            MyDialogs.showError(RouteInfoActivity.this, "Cancelar Rota", "Erro: " + super.taskException.getLocalizedMessage());
                        }
                    }
                });
            } else {
                removeLocalRoute();
            }
        } catch (Exception e) {
            MyLog.log(e);
        }

    }

    // remove order
    private void removeLocalOrder(RemoveOrderEvent event) {
        // remove from database
        if (this.ordersInfo.contains(event.order)) {
            this.ordersInfo.remove(event.order);
        }
        this.dao.saveObj("current_orders", this.ordersInfo);

        if (!this.ordersInfo.isEmpty()) {
            // remove from route calculator
            this.routeCalculator.removeOrderFromRoute(event.order);
            this.mapViewer.removeOrderMarker(event.order);
            this.placeViewer.removeOrderFromList(event.order);

            this.requestLastLocation();
        } else {
            // cancelar rota
            cancelRoute(true, !event.wasForProximity);
        }
    }

    private void doRemove(final RemoveOrderEvent event) {
        MyLog.i("remove order: " + event.order);
        if (event.notifyToServer) {
            // update server
            ExecuteTask.executeTask(new TaskDelegator() {
                @Override
                public void doInBackground() throws Exception {
                    JSONConverter conv = new JSONConverter();

                    EventChangeStatusOrder cancel = new EventChangeStatusOrder();

                    cancel.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                    cancel.setTo(GRXEvent.TO_TYPE.TO_USER);
                    cancel.setType(GRXEvent.EVENT_TYPE.REAL_TIME);


                    cancel.setOrderId(event.order.getOrderId());
                    cancel.setNewStatus(OrderStatus.ORDER_STATUS.NEW);
                    cancel.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());

                    EventMessage message = new EventMessage();
                    message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
                    message.setJsonString(conv.toJson(cancel));

                    HttpClient.getRef().sendEvent(message);
                }

                @Override
                public void onPostExecute() {
                    if (super.taskException == null) {
                        removeLocalOrder(event);
                    } else {
                        MyLog.log(super.taskException);
                        MyDialogs.showError(RouteInfoActivity.this, "Remover Pedido", "Erro: " + super.taskException.getLocalizedMessage());
                    }
                }
            });
        } else {
            removeLocalOrder(event);
        }
    }

    // remove order
    // main function to remove order, from app and server
    @Subscribe
    public void processRemoveOrder(final RemoveOrderEvent event) {
        try {

            MyLog.i("remove event order: " + event.order.getOrderType());
            if (event.order.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE) && !event.wasForProximity) {
                ExchangeOrderStatusDialog dialog = new ExchangeOrderStatusDialog(RouteInfoActivity.this, new ExchangeOrderStatusDialog.Callback() {
                    @Override
                    public void processYes(SectorInfo base, boolean sendToServer) {
                        if (sendToServer) {
                            ExecuteTask.executeTask(new TaskDelegator() {

                                private String action;

                                @Override
                                public void doInBackground() throws Exception {
                                    JSONConverter conv = new JSONConverter();

                                    EventChangeStatusOrder split = new EventChangeStatusOrder();

                                    split.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                                    split.setTo(GRXEvent.TO_TYPE.TO_USER);
                                    split.setType(GRXEvent.EVENT_TYPE.REAL_TIME);

                                    this.action = base.getSectorId();

                                    MyLog.i("this.action: " + this.action);

                                    split.setAction(this.action);


                                    split.setOrderId(event.order.getOrderId());
                                    split.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());

                                    EventMessage message = new EventMessage();
                                    message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
                                    message.setJsonString(conv.toJson(split));

                                    HttpClient.getRef().sendEvent(message);
                                }

                                @Override
                                public void onPostExecute() {
                                    if (super.taskException == null) {
                                        RouteInfoActivity.this.doRemove(event);
                                    } else {
                                        MyLog.log(super.taskException);
                                        MyDialogs.showError(RouteInfoActivity.this, "Cancelar Pedido", super.taskException.getLocalizedMessage());
                                    }
                                }
                            });
                        }


                    }
                });

                dialog.show();
            } else {
                RouteInfoActivity.this.doRemove(event);
            }


        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    // receive location updates
    @Subscribe
    synchronized public void updateMapLocation(GeolocationsEvent event) {
        if (this.routeCalculator != null) {
            GeoLocation firstLocation = event.locations.get(0);
            ExecuteTask.executeTask(new CalculateRouteTaskDelegator(new LatLng(firstLocation.getLatitude(), firstLocation.getLongitude()), true, true));
        } else {
            this.mapViewer.updateMapLocation(event.locations);
        }
    }


    private void showContainers() {
        dao.retrieveJSONObject("base", SectorInfo.class, new Callback<SectorInfo>() {
            @Override
            public void retrieve(SectorInfo base) {
                ContainerSelectionDialog dialog = new ContainerSelectionDialog(RouteInfoActivity.this, base, new ContainerSelectionDialog.Callback() {
                    @Override
                    public void processYes(List<ContainerInfo> containers) {
                        MyLog.i("containers: " + containers);
                        dao.saveAsJSON("containers", containers);
                    }

                    @Override
                    public void processNo() {

                    }
                });
                dialog.setCancelable(false);
                dialog.show();


                // Initialize a new window manager layout parameters
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

                // Copy the alert dialog window attributes to new layout parameter instance
                layoutParams.copyFrom(dialog.getWindow().getAttributes());

                // Set the width and height for the layout parameters
                // This will bet the width and height of alert dialog
                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

                dialog.getWindow().setAttributes(layoutParams);
            }
        });


    }

    // waiting for route
    @Subscribe
    public void processWaitingRoute(WaitingRouteEvent event) {
        MyLog.i("waiting event status: " + event.status);
        if (event.status.equals("waiting")) {
            this.watingMessage = event.message;
        } else if (event.status.equals("ok_waiting")) {
            while (getRouteInfo() == null || getOrdersInfo() == null) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // remover runnable??... NOOOOO
                    updateViews();
                    requestLastLocation();
                }
            });

            this.watingMessage = null;
        }
    }


    // receive errors
    @Subscribe
    public void notifyError(ErrorRouteEvent error) {
        MyLog.i("error: " + error);
        this.errorResult = error.messageError;
    }

    private List<ContainerInfo> containers = null;

    private void confirmOrder(final ProximityEvent event) {

        CheckoutContainerSelectionDialog dialog = new CheckoutContainerSelectionDialog(RouteInfoActivity.this, this.dao, new CheckoutContainerSelectionDialog.Callback() {
            @Override
            public void processYes(List<ContainerInfo> containers) {
                ExecuteTask.executeTask(new TaskDelegator() {
                    @Override
                    public void doInBackground() throws Exception {

                        JSONConverter conv = new JSONConverter();

                        EventChangeStatusOrder status = new EventChangeStatusOrder();
                        status.setNewStatus(OrderStatus.ORDER_STATUS.FINISHED);

                        status.setOrderId(event.order.getOrderId());
                        status.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                        status.setTo(GRXEvent.TO_TYPE.TO_USER);
                        status.setType(GRXEvent.EVENT_TYPE.REAL_TIME);

                        if (!containers.isEmpty()) {
                            status.setContainer_id(containers.get(0).getId());
                        }

                        EventMessage message = new EventMessage();
                        message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
                        message.setJsonString(conv.toJson(status));

                        HttpClient.getRef().sendEvent(message);

                    }

                    @Override
                    public void onPostExecute() {
                        if (super.taskException == null) {
                            MyDialogs.showInformation(RouteInfoActivity.this, "Check-in Pedido", "Pedido finalizado: " + event.order.getOrderId().getOrder_id(), new MyDialogs.MyDialogListener() {
                                @Override
                                public void processClose() {
                                    try {
                                        RemoveOrderEvent event1 = new RemoveOrderEvent();
                                        event1.order = event.order;
                                        event1.notifyToServer = false;
                                        event1.wasForProximity = true;
                                        processRemoveOrder(event1);
                                    } catch (Exception e) {
                                        MyLog.log(e);
                                        MyDialogs.showError(RouteInfoActivity.this, "Cancelar Pedido", "Erro: " + e.getLocalizedMessage());
                                    }
                                }
                            });
                        } else {
                            MyLog.log(super.taskException);
                            MyDialogs.showError(RouteInfoActivity.this, "Check-in Pedido", "Erro:" + super.taskException.getLocalizedMessage());
                        }
                    }
                });
            }


        });
        dialog.setCancelable(false);
        dialog.show();

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(dialog.getWindow().getAttributes());

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.getWindow().setAttributes(layoutParams);


    }

    @Subscribe
    public void receiveProximity(final ProximityEvent event) {
        MyLog.i("receiveProximity: " + event);

        if (event.bringToFront) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PackagesUtils.bringApplicationToFront(RouteInfoActivity.this);
                }
            });
        }

        if (event.order.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE)) {
            ExchangeOrderStatusDialog dialog = new ExchangeOrderStatusDialog(RouteInfoActivity.this, new ExchangeOrderStatusDialog.Callback() {
                @Override
                public void processYes(SectorInfo base, boolean sendToServer) {
                    if (sendToServer) {
                        ExecuteTask.executeTask(new TaskDelegator() {
                            public String action;

                            @Override
                            public void doInBackground() throws Exception {
                                JSONConverter conv = new JSONConverter();

                                EventChangeStatusOrder split = new EventChangeStatusOrder();

                                split.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                                split.setTo(GRXEvent.TO_TYPE.TO_USER);
                                split.setType(GRXEvent.EVENT_TYPE.REAL_TIME);

                                this.action = base.getSectorId();

                                MyLog.i("this.action from proximity: " + this.action);

                                split.setAction(this.action);


                                split.setOrderId(event.order.getOrderId());
                                split.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());

                                EventMessage message = new EventMessage();
                                message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
                                message.setJsonString(conv.toJson(split));

                                HttpClient.getRef().sendEvent(message);
                            }
                        });
                    }

                    RouteInfoActivity.this.confirmOrder(event);
                }
            });

            dialog.show();
        } else {
            RouteInfoActivity.this.confirmOrder(event);
        }


    }

    private void retrieveOldRouteInfo() {
        dao.retrieveObject("current_route_as_json", String.class, new Callback<String>() {
            @Override
            public void retrieve(String route_str) {

                RouteInfoWrapper route = null;
                try {
                    JSONConverter conv = new JSONConverter();
                    route = conv.toObject(route_str, RouteInfoWrapper.class);
                } catch (Exception e) {
                    MyLog.log(e);
                }

                MyLog.i("route loaded: " + route);
                if (route != null) {
                    if (routeInfo != null && routeInfo.getRoute().getId().equals(route.getRoute().getId())) {
                        MyLog.i("########## skipping retrieved route ##########");
                        return;
                    }

                    routeInfo = route;

                    dao.retrieveAsListObject("current_orders", OrderInfo.class, new Callback<OrderInfo>() {
                        @Override
                        public void retrieve(List list) {
                            MyLog.i("orders loaded: " + list);

                            if (ordersInfo != null) {
                                MyLog.i("########## skipping retrieved orders ##########");
                                return;
                            }

                            if (list != null) {
                                ordersInfo = list;

                                if (routeInfo != null && ordersInfo != null) {

                                    ExecuteTask.executeTask(new TaskDelegator() {
                                        @Override
                                        public void doInBackground() throws Exception {
                                            JSONConverter conv = new JSONConverter();

                                            EventChangeStatusRoute eventChange = new EventChangeStatusRoute();
                                            eventChange.setRoute_id(RouteInfoActivity.this.routeInfo.getRoute().getId());
                                            eventChange.setFromUserId(RouteInfoActivity.this.userInfo.getUserId());
                                            eventChange.setTo(GRXEvent.TO_TYPE.TO_USER);
                                            eventChange.setType(GRXEvent.EVENT_TYPE.REAL_TIME);
                                            RouteStatus newStatus = new RouteStatus();
                                            newStatus.setStatus(RouteStatus.ROUTE_STATUS.SELECT);
                                            newStatus.setTimeDate(Calendar.getInstance().getTime());
                                            eventChange.setNewStatus(newStatus);

                                            EventMessage message = new EventMessage();
                                            message.setEventClass(EventChangeStatusRoute.class.getCanonicalName());
                                            message.setJsonString(conv.toJson(eventChange));

                                            HttpClient.getRef().sendEvent(message);
                                        }

                                        @Override
                                        public void onPostExecute() {
                                            if (super.taskException == null) {
                                                // initialize route calculator
                                                routeCalculator = new RouteCalculator(ordersInfo);
                                                requestLastLocation();

                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        updateViews();
                                                    }
                                                });
                                            } else {
                                                routeCalculator = null;
                                                MyLog.log(super.taskException);
                                                MyDialogs.showError(RouteInfoActivity.this, "Carregando Rota", "Erro carregando rota: " + this.taskException.getLocalizedMessage());
                                            }
                                        }
                                    });


                                }
                            }
                        }
                    });

                    dao.retrieveObject("current_truck", TruckId.class, new Callback<TruckId>() {
                        @Override
                        public void retrieve(TruckId obj) {
                            RouteInfoActivity.this.truckId = obj;
                        }
                    });
                }
            }
        });
    }


    synchronized public void updateViews() {

        // update order list
        try {
            MyLog.i("populate orders: " + ordersInfo);
            placeViewer.populateOrderList(ordersInfo);
        } catch (Exception e) {
            MyLog.log(e);
            MyDialogs.showError(RouteInfoActivity.this, "Procurando pedidos", "Erro: " + e.getLocalizedMessage());
        }

        // draw orders on map
        try {
            MyLog.i("drawing orders: " + ordersInfo);
            mapViewer.drawOrders(ordersInfo);
        } catch (Exception e) {
            MyLog.log(e);
            MyDialogs.showError(RouteInfoActivity.this, "Procurando pedidos", "Erro: " + e.getLocalizedMessage());
        }

        // recover last location for draw route
        this.requestLastLocation();

        /*// show route on map
        try {
            // do nothing without location
            if (LocationUpdater.getRef().getLastKnowLocation() != null) {
                calculateAndDrawRoute(new LatLng(LocationUpdater.getRef().getLastKnowLocation().getLatitude(), LocationUpdater.getRef().getLastKnowLocation().getLongitude()), true, false);
            }
        } catch (Exception e) {
            MyLog.log(e);
            MyDialogs.showError(RouteInfoActivity.this, "Procurando pedidos", "Erro: " + e.getLocalizedMessage());
        }*/


    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////


    /* (non-Javadoc)
     * @see com.activity.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        // super.onClick(v);
        if (v.getId() == R.id.tab1)
            pager.setCurrentItem(0, true);
        else if (v.getId() == R.id.tab2)
            pager.setCurrentItem(1, true);
    }

    /**
     * Initialize the tabs. You can write your code related to Tabs.
     */
    private void initTabs() {
        findViewById(R.id.tab1).setOnClickListener(this);
        findViewById(R.id.tab2).setOnClickListener(this);
        setCurrentTab(0);
    }

    /**
     * Sets the current selected tab. Called whenever a Tab is selected either
     * by clicking on Tab button or by swiping the ViewPager. You can write your
     * code related to tab selection actions.
     *
     * @param page the current page of ViewPager
     */
    private void setCurrentTab(int page) {
        if (currentTab != null)
            currentTab.setEnabled(true);
        if (page == 0)
            currentTab = findViewById(R.id.tab1);
        else if (page == 1)
            currentTab = findViewById(R.id.tab2);
        currentTab.setEnabled(false);

    }

    private int current_page;

    /**
     * Initialize the ViewPager. You can customize this method for writing the
     * code related to view pager actions.
     */
    private void initPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        pager.addOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int page) {
                RouteInfoActivity.this.current_page = page;

                if (page == 1) {
                    textViewAddress.setVisibility(View.GONE);
                    textViewOrder.setVisibility(View.GONE);
                    menuFab.setVisibility(View.GONE);

                    MyLog.i("@@@ invisible 3 @@@");
                    btnNavigate.setVisibility(View.INVISIBLE);
                    dragViewPrice.setVisibility(View.INVISIBLE);

                } else {
                    textViewAddress.setVisibility(View.VISIBLE);
                    textViewOrder.setVisibility(View.VISIBLE);
                    menuFab.setVisibility(View.VISIBLE);
                    if (routeCalculator != null) {
                        MyLog.i("@@@ visible @@@");
                        dragViewPrice.setVisibility(View.VISIBLE);
                        btnNavigate.setVisibility(View.VISIBLE);


                    } else {
                        MyLog.i("@@@ invisible @@@");
                        btnNavigate.setVisibility(View.INVISIBLE);
                        dragViewPrice.setVisibility(View.INVISIBLE);
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        menuFab.close(true);
                    }
                });

                setCurrentTab(page);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        pager.setAdapter(new TabPageAdapter(getSupportFragmentManager()));
    }

    public void requestLastLocation() {
        this.checkLocationPermission();
        this.fusedProvider.getLastLocation().addOnSuccessListener(this, new LocationOnSuccessListener());
        this.fusedProvider.getLastLocation().addOnFailureListener(this, new LocationOnFailureListener());
    }

    public void showMessage(final String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewOrder.setText(message);
                textViewAddress.setText("");
            }
        });
    }

    /**
     * The Class DummyPageAdapter is a dummy pager adapter for ViewPager. You
     * can customize this adapter as per your needs.
     */
    private class TabPageAdapter extends FragmentPagerAdapter {

        private int currentPosition;

        /**
         * Instantiates a new dummy page adapter.
         *
         * @param fm the FragmentManager
         */
        public TabPageAdapter(FragmentManager fm) {
            super(fm);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
         */
        @Override
        public Fragment getItem(int pos) {
            this.currentPosition = pos;
            if (pos == 0) {
                mapViewer = new MapViewer();
                eventBus.register(mapViewer);
                return mapViewer;
            }

            if (pos == 1) {
                placeViewer = new RoutePoints();
                return placeViewer;
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return 2;
        }


        public int getCurrentPosition() {
            return currentPosition;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            View decorView = getWindow().getDecorView();

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        }
    }

    /* (non-Javadoc)
     * @see com.newsfeeder.custom.CustomActivity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.search_exp, menu);

        //    if (getActionBar().getTitle() == getString(R.string.nearby))
        //      menu.findItem(R.id.menu_loc).setVisible(false);

        // setupSearchView(menu);
        return true;
    }

    /* (non-Javadoc)
     * @see com.activity.custom.CustomActivity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_loc) {
            startActivity(new Intent(this, RouteInfoActivity.class).putExtra("title",
                    getString(R.string.nearby)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Setup the up search view for ActionBar search. The current implementation
     * simply search for Videos from Youtube. You can customize this code to
     * search from your API or any TV API.
     *
     * @param menu the ActionBar Menu
     */
    protected void setupSearchView(Menu menu) {
        searchView = (SearchView) menu.findItem(R.id.menu_search)
                .getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint("Search...");
        searchView.requestFocusFromTouch();
        searchView.setOnQueryTextListener(new OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });

        setupSearchViewTheme(searchView);
    }

    /**
     * Sets the up search view theme.
     *
     * @param searchView the new up search view theme
     */
    protected void setupSearchViewTheme(SearchView searchView) {
        int id = searchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        EditText searchPlate = (EditText) searchView.findViewById(id);
        searchPlate.setHintTextColor(getResources().getColor(R.color.white));

        id = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ((ImageView) searchView.findViewById(id))
                .setImageResource(R.drawable.ic_close);

        id = searchView.getContext().getResources()
                .getIdentifier("android:id/search_mag_icon", null, null);
        ((ImageView) searchView.findViewById(id))
                .setImageResource(R.drawable.ic_search_small);

        id = searchView.getContext().getResources()
                .getIdentifier("android:id/search_plate", null, null);
        searchView.findViewById(id).setBackgroundResource(
                R.drawable.edittext_bg_white);
        try {
            id = searchView.getContext().getResources()
                    .getIdentifier("android:id/search_button", null, null);
            ((ImageView) searchView.findViewById(id))
                    .setImageResource(R.drawable.icon_search);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    /*private class DialogConfirmCallback implements MyDialogs.Callback {
        @Override
        public void processYes() {

            RouteInfoActivity.this.routeInfo = null;
            RouteInfoActivity.this.ordersInfo = null;

            MyDialogs.showConfirm(RouteInfoActivity.this, "Procurar Pedidos", "Iniciar na Base?", new StartBaseCallback());
        }

        @Override
        public void processNo() {

        }

    }*/

    /*private class StartBaseCallback implements MyDialogs.Callback {

        @Override
        public void processYes() {
            TruckSelectionDialog dialog = new TruckSelectionDialog(RouteInfoActivity.this, RouteInfoActivity.this.userInfo.getUserId(), new TruckSelectionCallback(true));
            dialog.show();
            dialog.getWindow().setLayout(500, 600);

        }

        @Override
        public void processNo() {
            TruckSelectionDialog dialog = new TruckSelectionDialog(RouteInfoActivity.this, RouteInfoActivity.this.userInfo.getUserId(), new TruckSelectionCallback(false));
            dialog.show();
            dialog.getWindow().setLayout(500, 600);
        }

    }*/


    private class TruckSelectionCallback implements TruckSelectionDialog.Callback {


        @Override
        public void processYes(final TruckInfo truck) {

            dao.saveObj("current_truck", truck.getTruckId());

            MyDialogs.showConfirm(RouteInfoActivity.this, "Procurar Pedidos", "Voce está saindo da Base?", new MyDialogs.Callback() {
                @Override
                public void processYes() {
                    BaseSelectionDialog dialog = new BaseSelectionDialog(RouteInfoActivity.this,"Base Inicial", RouteInfoActivity.this.userInfo.getUserId(), new BaseSelectionCallbackStart(true, truck));
                    dialog.show();

                    // Initialize a new window manager layout parameters
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

                    // Copy the alert dialog window attributes to new layout parameter instance
                    layoutParams.copyFrom(dialog.getWindow().getAttributes());

                    // Set the width and height for the layout parameters
                    // This will bet the width and height of alert dialog
                    layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

                    dialog.getWindow().setAttributes(layoutParams);
                    //  dialog.getWindow().setLayout(500, 600);

                }

                @Override
                public void processNo() {
                    QtdeContainerDialog dialog = new QtdeContainerDialog(RouteInfoActivity.this, RouteInfoActivity.this.userInfo.getUserId(), new QtdeContainerCallback(false, truck));
                    dialog.show();

                    // Initialize a new window manager layout parameters
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

                    // Copy the alert dialog window attributes to new layout parameter instance
                    layoutParams.copyFrom(dialog.getWindow().getAttributes());

                    // Set the width and height for the layout parameters
                    // This will bet the width and height of alert dialog
                    layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

                    dialog.getWindow().setAttributes(layoutParams);
                    //  dialog.getWindow().setLayout(500, 600);

                }
            });
        }

        @Override
        public void processNo() {

        }
    }


    private class QtdeContainerCallback implements QtdeContainerDialog.Callback{

        private boolean startBase;
        private TruckInfo truck;


        public QtdeContainerCallback(boolean startBase, TruckInfo truck) {
            this.startBase = startBase;
            this.truck = truck;
        }

        @Override
        public void processYes(int qtde) {
            BaseSelectionDialog dialog = new BaseSelectionDialog(RouteInfoActivity.this, "Base Final",RouteInfoActivity.this.userInfo.getUserId(), new BaseSelectionCallbackFinal(false, truck));
            dialog.show();

            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(dialog.getWindow().getAttributes());

            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

            dialog.getWindow().setAttributes(layoutParams);
            //  dialog.getWindow().setLayout(500, 600);
        }

        @Override
        public void processNo() {

        }
    }

    private class BaseSelectionCallbackStart implements BaseSelectionDialog.Callback {

        private boolean startBase;
        private TruckInfo truck;


        public BaseSelectionCallbackStart(boolean startBase, TruckInfo truck) {
            this.startBase = startBase;
            this.truck = truck;
        }

        @Override
        public void processYes(SectorInfo base) {
            BaseSelectionDialog dialog = new BaseSelectionDialog(RouteInfoActivity.this, "Base Final",RouteInfoActivity.this.userInfo.getUserId(), new BaseSelectionCallbackFinal(false, truck));
            dialog.show();

            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(dialog.getWindow().getAttributes());

            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

            dialog.getWindow().setAttributes(layoutParams);
            //  dialog.getWindow().setLayout(500, 600);
        }

        @Override
        public void processNo() {

        }
    }

    private class BaseSelectionCallbackFinal implements BaseSelectionDialog.Callback {

        private boolean startBase;
        private TruckInfo truck;


        public BaseSelectionCallbackFinal(boolean startBase, TruckInfo truck) {
            this.startBase = startBase;
            this.truck = truck;
        }

        @Override
        public void processYes(SectorInfo base) {
            MyLog.i("base:" + base.getSectorId());
            dao.saveAsJSON("base", base);

            ExecuteTask.executeTask(new FindTasksDelegator(RouteInfoActivity.this, userInfo.getUserId(), startBase, truck.getTruckId(), base.getSectorId()));

            /*ContainerSelectionDialog dialog = new ContainerSelectionDialog(RouteInfoActivity.this, base, new ContainerCallback(this.startBase, this.truck, base));
            dialog.show();

            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(dialog.getWindow().getAttributes());

            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

            dialog.getWindow().setAttributes(layoutParams);*/
        }

        @Override
        public void processNo() {

        }
    }


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    private class LocationOnFailureListener implements OnFailureListener {

        @Override
        public void onFailure(@NonNull Exception e) {

        }
    }

    private class LocationOnSuccessListener implements OnSuccessListener<Location> {
        @Override
        public void onSuccess(Location location) {

            if (location != null) {
                MyLog.i("lat: " + location.getLatitude() + " lng: " + location.getLongitude());
                GeolocationsEvent event = new GeolocationsEvent();
                event.locations = new ArrayList<>();
                GeoLocation geolocation = new GeoLocation();
                geolocation.setLatitude(location.getLatitude());
                geolocation.setLongitude(location.getLongitude());
                event.locations.add(geolocation);
                eventBus.post(event);
            }
        }


    }

    private class MenuFabClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (currentTab.getId() == R.id.tab1) {
                MyLog.i("click on tab 1?: " + currentTab.getId());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (routeInfo == null) {
                            menuFab.close(true);
                            errorResult = null;
                            routeInfo = null;
                            ordersInfo = null;

                            TruckSelectionDialog dialog = new TruckSelectionDialog(RouteInfoActivity.this, RouteInfoActivity.this.userInfo.getUserId(), new TruckSelectionCallback());
                            dialog.show();

                            // Initialize a new window manager layout parameters
                            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

                            // Copy the alert dialog window attributes to new layout parameter instance
                            layoutParams.copyFrom(dialog.getWindow().getAttributes());

                            // Set the width and height for the layout parameters
                            // This will bet the width and height of alert dialog
                            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

                            dialog.getWindow().setAttributes(layoutParams);

                        } else {
                            if (menuFab.isOpened()) {
                                menuFab.close(true);
                            } else {
                                menuFab.open(true);
                            }
                        }
                    }
                });

            }

            if (currentTab.getId() == R.id.tab2) {
                MyLog.i("click on tab 2?: " + currentTab.getId());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (menuFab.isOpened()) {
                            menuFab.close(true);
                        } else {
                            menuFab.open(true);
                        }
                    }
                });
            }
        }

    }


    private class CalculateRouteTaskDelegator extends TaskDelegator {
        private final LatLng latLng;
        private final boolean updatePosition;
        private SpotsDialog progressDialog;
        private boolean quiet;

        public CalculateRouteTaskDelegator(LatLng latLng, boolean updatePosition, boolean quiet) {
            this.latLng = latLng;
            this.updatePosition = updatePosition;
            this.quiet = quiet;

            if (!this.quiet) {
                this.progressDialog = new SpotsDialog(RouteInfoActivity.this, R.style.CustomProgressRoute);
                this.progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }

        @Override
        public void onPreExecute() {
            if (!this.quiet) {
                this.progressDialog.show();
            }
        }

        @Override
        public void doInBackground() throws Exception {
            MyLog.i("start time: " + Calendar.getInstance().getTime());
            if (routeCalculator.getMainRoutePoints() == null) {
                MyLog.i("calculating main route");
                routeCalculator.calculateMainRoute(latLng);
            } else {
                MyLog.i("calculating sub-route");
                routeCalculator.calculateSubRouteFromLocation(latLng);
            }
            MyLog.i("end time: " + Calendar.getInstance().getTime());
        }

        @Override
        public void onPostExecute() {
            if (super.taskException != null) {
                if (!this.quiet) {
                    this.progressDialog.dismiss();
                }

                MyLog.log(super.taskException);
                MyDialogs.showError(RouteInfoActivity.this, "Calculando Rota", "Erro:" + super.taskException.getLocalizedMessage());

                return;
            }

            // update current order
            try {
                MyLog.i("@@@ update order text @@@");
                textViewOrder.setText("Ordem No: " + routeCalculator.getCurrentOrder().getOrderId().getOrder_id());
                textViewAddress.setText(routeCalculator.getCurrentOrder().getAddress().getAddress().asAddressString());
                if (current_page == 0) {
                    MyLog.i("@@@ showing control @@@");
                    dragViewPrice.setVisibility(View.VISIBLE);
                    btnNavigate.setVisibility(View.VISIBLE);


                } else {
                    MyLog.i("@@@ hide control @@@");
                    btnNavigate.setVisibility(View.INVISIBLE);
                    dragViewPrice.setVisibility(View.INVISIBLE);
                }


                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                slideLayout.refreshDrawableState();
            } catch (Exception e) {
                MyLog.log(e);
            }

            // draw driver location
            try {
                List<com.google.android.gms.maps.model.LatLng> toDrawPoint = routeCalculator.getToDrawPoints();

                if (toDrawPoint == null || toDrawPoint.isEmpty()) {
                    return;
                }

                mapViewer.drawRouteFromPoints(toDrawPoint);

                if (this.updatePosition) {
                    List<GeoLocation> locations = new ArrayList<>();
                    com.google.android.gms.maps.model.LatLng firstPoint = toDrawPoint.get(0);

                    GeoLocation location = new GeoLocation();
                    location.setLatitude(firstPoint.latitude);
                    location.setLongitude(firstPoint.longitude);

                    locations.add(location);

                    mapViewer.updateMapLocation(locations);
                }
            } catch (IllegalStateException e) {
                MyLog.i("IllegalStateException: " + e + " message: " + e.getLocalizedMessage());
            } catch (Exception e) {
                MyLog.i("exception: " + e + " message: " + e.getLocalizedMessage());
            }

            if (!this.quiet) {
                this.progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MyLog.i("requestCode: " + requestCode);
        MyLog.i("permissions: " + (permissions.length > 0 ? permissions[0] : "VAZIO"));

        if (requestCode == 50) {
            Intent i = new Intent(this, SurfaceCameraActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

}

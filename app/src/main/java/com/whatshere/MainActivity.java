package com.whatshere;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.whatshere.custom.CustomActivity;
import com.whatshere.custom.CustomFragment;
import com.whatshere.http.HttpClient;
import com.whatshere.messaging.GRXTrackingIntanceIdService;
import com.whatshere.model.Data;
import com.whatshere.persistence.DAO;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.ui.LeftNavAdapter;
import com.whatshere.ui.MainFragment;
import com.whatshere.ui.MapViewer;
import com.whatshere.ui.Profile;
import com.whatshere.ui.Settings;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.MyLog;

import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.TokenInfo;

/**
 * The Activity MainActivity will launched after the Login and it is the
 * Home/Base activity of the app which holds all the Fragments and also show the
 * Sliding Navigation drawer. You can write your code for displaying actual
 * items on Drawer layout.
 */
public class MainActivity extends CustomActivity {

    /**
     * The drawer layout.
     */
    private DrawerLayout drawerLayout;

    /**
     * ListView for left side drawer.
     */
    private ListView drawerLeft;

    /**
     * The drawer toggle.
     */
    private ActionBarDrawerToggle drawerToggle;

    /**
     * Event bus
     **/
    private EventBus eventBus;

    /**
     * HTTP Client
     **/
    private HttpClient client;

    /**
     * DAO
     */

    private DAO dao;

    // user
    private UserInfo userInfo;

    /* (non-Javadoc)
     * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dao = new DAO();

        setContentView(R.layout.activity_main);
        setupDrawer();
        setupContainer(1);

        Intent serviceIntent = new Intent(this, GRXTrackingIntanceIdService.class);
        startService(serviceIntent);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.userInfo = (UserInfo) extras.getSerializable("userinfo");
        }

        this.eventBus = EventBusIntance.getEventBus();
        this.eventBus.register(this);

        this.client = HttpClient.getRef();

        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_theme)));

        {
            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
            String token = FirebaseInstanceId.getInstance().getToken();
            MyLog.i("token: " + token);
        }


    }

    @Subscribe
    public void registerToken(TokenInfo token) {
        MyLog.i("token from eventbus: " + token);
        try {
            this.client.registerTokenFirebase(token);
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    /**
     * Setup the drawer layout. This method also includes the method calls for
     * setting up the Left & Right side drawers.
     */
    private void setupDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.closeDrawers();

        setupLeftNavDrawer();
    }

    /**
     * Setup the left navigation drawer/slider. You can add your logic to load
     * the contents to be displayed on the left side drawer. It will also setup
     * the Header and Footer contents of left drawer. This method also apply the
     * Theme for components of Left drawer.
     */
    @SuppressLint("InflateParams")
    private void setupLeftNavDrawer() {
        drawerLeft = (ListView) findViewById(R.id.left_drawer);

        View header = getLayoutInflater().inflate(R.layout.left_nav_header,
                null);

        drawerLeft.addHeaderView(header);

        final ArrayList<Data> al = new ArrayList<Data>();
        al.add(new Data(new String[]{"Rotas"}, new int[]{
                R.drawable.ic_nav1, R.drawable.ic_nav1}));
        al.add(new Data(new String[]{"Pendentes"}, new int[]{
                R.drawable.ic_nav2, R.drawable.ic_nav2}));
        al.add(new Data(new String[]{"Ajustes"}, new int[]{
                R.drawable.ic_nav3, R.drawable.ic_nav3}));
        al.add(new Data(new String[]{"Sair"}, new int[]{
                R.drawable.ic_nav4, R.drawable.ic_nav4}));

        final LeftNavAdapter adp = new LeftNavAdapter(this, al);
        drawerLeft.setAdapter(adp);
        drawerLeft.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View arg1, int pos,
                                    long arg3) {
                if (pos != 2)
                    adp.setSelection(pos - 1);
                drawerLayout.closeDrawers();
                setupContainer(pos);
            }
        });

    }

    /**
     * Setup the container fragment for drawer layout. This method will setup
     * the grid view display of main contents. You can customize this method as
     * per your need to display specific content.
     *
     * @param pos the new up container
     */
    private void setupContainer(int pos) {
        if (pos == 4) {
            dao.removeObject("userinfo");

            startActivity(new Intent(this, Login.class));
            finish();
        }
        CustomFragment f = null;
        String title = getString(R.string.app_name);

        if (pos == 0) {
            f = new Profile();
            title = "GRXTracking v1.0.1";
        } else if (pos == 1) {
            f = new MainFragment();
            {
                Intent intent = new Intent(this, RouteInfoActivity.class);
                if (this.userInfo != null) {
                    Bundle extras = new Bundle();
                    extras.putSerializable("userinfo", this.userInfo);
                    intent.putExtras(extras);
                }
                startActivity(intent);
            }

        } else if (pos == 2) {
            // startActivity(new Intent(this, RouteInfoActivity.class).putExtra("title",
            //                    "Favorites"));
        } else if (pos == 3) {
            //  f = new MapViewer();
            title = "Settings";
        }
        if (f == null)
            return;

        getActionBar().setTitle(title);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f).commit();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onPostCreate(android.os.Bundle)
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        drawerToggle.onConfigurationChanged(newConfig);
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomActivity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

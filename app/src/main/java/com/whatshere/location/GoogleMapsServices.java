package com.whatshere.location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.google.maps.DirectionsApi;
import com.google.maps.ElevationApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.RoadsApi;
import com.google.maps.android.PolyUtil;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.ElevationResult;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.whatshere.utils.MyLog;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.GeoLocation;

public class GoogleMapsServices {

    // private static Logger logger = Logger.getLogger(GoogleMapsServices.class.getCanonicalName());

    public static String API_KEY = "AIzaSyBG-8a072nSwUAZaA_f4cJibyJtonJG2ds";

    private GeoApiContext context;

    public GoogleMapsServices() {
        this.context = new GeoApiContext.Builder().apiKey(API_KEY).build();
    }

    public GeoLocation geoDecoding(Address addr) throws Exception {
        String address_str = addr.getXLgr() + " " + addr.getEndereco() + ", " + addr.getNro() + ", " + addr.getXBairro()
                + ", " + addr.getCidade() + ", " + addr.getXMun();
        address_str = address_str.replace("null", "");
        return this.geoDecoding(address_str);
    }

    public GeoLocation geoDecoding(String address) throws Exception {

        GeoLocation geoLocation = new GeoLocation();

        GeocodingResult[] results = GeocodingApi.geocode(this.context, address).await();
        for (GeocodingResult result : results) {
            geoLocation.setLatitude(result.geometry.location.lat);
            geoLocation.setLongitude(result.geometry.location.lng);

            break;
        }

        return geoLocation;
    }

    public Address reverseDecoding(GeoLocation location) throws Exception {
        Address address = new Address();

        LatLng location2 = new LatLng(location.getLatitude(), location.getLongitude());
        GeocodingResult[] results = GeocodingApi.reverseGeocode(this.context, location2).await();
        for (GeocodingResult result : results) {

            AddressComponent[] address_comp = result.addressComponents;
            for (AddressComponent component : address_comp) {
                System.out.println(component.longName + " " + component.types[0]);
                List<AddressComponentType> types = Arrays.asList(new AddressComponentType[]{component.types[0]});
                if (types.contains(AddressComponentType.STREET_NUMBER)) {
                    address.setNro(component.longName.replace("-", " ").toUpperCase());
                }
                if (types.contains(AddressComponentType.ROUTE)) {
                    address.setEndereco(component.longName.toUpperCase());
                }
                if (types.contains(AddressComponentType.POLITICAL)) {
                    address.setXBairro(component.longName.toUpperCase());
                }

                if (types.contains(AddressComponentType.LOCALITY)) {
                    address.setXMun(component.longName.toUpperCase());
                }

                if (types.contains(AddressComponentType.POSTAL_CODE)) {
                    address.setCEP(component.longName.toUpperCase());
                }
            }

            break;
        }

        MyLog.i("address:" + address.getXBairro());
        return address;
    }


    public List<com.google.android.gms.maps.model.LatLng> extractRouteInfo(LatLng origin, LatLng destination) throws Exception {

        List<com.google.android.gms.maps.model.LatLng> coordenates = new ArrayList<>();


        DirectionsResult calculatedRoutes = DirectionsApi.newRequest(context).mode(TravelMode.DRIVING)
                .origin(origin).destination(destination).await();

        DirectionsRoute[] routes = calculatedRoutes.routes;
        for (DirectionsRoute route : routes) {


            MyLog.i("route: " + route);
            for (DirectionsLeg leg : route.legs) {

                // MyLog.i("leg: " + leg + " distance: " + leg.distance.inMeters);
                for (DirectionsStep step : leg.steps) {
                    // MyLog.i("step: " + step);

                    List<com.google.android.gms.maps.model.LatLng> points = PolyUtil.decode(step.polyline.getEncodedPath());

                    for (com.google.android.gms.maps.model.LatLng result : points) {
                        coordenates.add(result);
                    }
                }
                // just first leg
                break;
            }
            // just first route
            break;
        }
        return coordenates;
    }

    public void calculateRoute(String origin, String destination) throws Exception {

        DirectionsResult calculatedRoutes = DirectionsApi.newRequest(context).mode(TravelMode.DRIVING).origin(origin)
                .destination(destination).await();
        DirectionsRoute[] rr = calculatedRoutes.routes;
        for (DirectionsRoute r : rr) {
            for (DirectionsLeg leg : r.legs) {
                System.out.println("leg: " + leg.distance.inMeters);

                for (DirectionsStep step : leg.steps) {

                    System.out.println(step.polyline.getEncodedPath());

                    EncodedPolyline encodedPolyline = new EncodedPolyline(step.polyline.getEncodedPath());
                    ElevationResult[] result = ElevationApi.getByPoints(context, encodedPolyline).await();

                    for (ElevationResult rrr : result) {

                    }
                }

                System.out.println(" l --------------------------------------");

            }

            System.out.println(" r --------------------------------------");
        }
    }

    public static void main(String[] args) {

        try {
            GoogleMapsServices c = new GoogleMapsServices();
            GeoLocation location = new GeoLocation();
            location.setLatitude(-23.58847669263849);
            location.setLongitude(-46.54373703848421);
            Address addresss = c.reverseDecoding(location);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

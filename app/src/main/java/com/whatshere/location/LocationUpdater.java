package com.whatshere.location;

import com.google.android.gms.maps.model.LatLng;
import com.google.common.eventbus.Subscribe;
import com.google.maps.android.SphericalUtil;
import com.whatshere.eventbus.LocationEvent;
import com.whatshere.http.HttpClient;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.GeolocationsEvent;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.transport.EventMessage;

public class LocationUpdater {

    private static LocationUpdater instance = null;

    private static double MAX_DESLOCATED_DISTANCE = 10d; // meters
    private static int MAX_TIMES_WITHOUT_SEND = 5;

    private int times_received;
    private long last_sent;
    private List<GeoLocation> locations;
    private GeoLocation lastKnowLocation;

    public static LocationUpdater getRef() {
        if (instance == null) {
            instance = new LocationUpdater();
        }
        return instance;
    }

    private LocationUpdater() {
        this.locations = new ArrayList<>();
    }


    public GeoLocation getLastKnowLocation() {
        return lastKnowLocation;
    }

    @Subscribe
    public void updateLocation(LocationEvent event) {
        // MyLog.i("LocationUpdater updateLocation:" + locationResult);

        this.times_received++;

        GeoLocation location = new GeoLocation();
        location.setLatitude(event.result.getLastLocation().getLatitude());
        location.setLongitude(event.result.getLastLocation().getLongitude());

        if (this.lastKnowLocation == null) {
            this.lastKnowLocation = location;
            this.locations.add(location);
            postLocation(event.userId);
        } else {
            double deslocated_distance = SphericalUtil.computeDistanceBetween(new LatLng(this.lastKnowLocation.getLatitude(), this.lastKnowLocation.getLongitude()), new LatLng(location.getLatitude(), location.getLongitude()));
            MyLog.i("deslocated_distance: " + deslocated_distance);
            if (deslocated_distance > MAX_DESLOCATED_DISTANCE) {
                this.lastKnowLocation = location;
                this.locations.add(location);
                postLocation(event.userId);
            }
        }
        /*if ((Calendar.getInstance().getTimeInMillis() - last_sent) > interval) {
            if (!locations.isEmpty()) {
                postLocation();
            }
        }*/

        // max 30 seconds without post location
        if (this.times_received >= MAX_TIMES_WITHOUT_SEND) {
            this.locations.add(location);
            postLocation(event.userId);
        }

    }

    public void postLocation(final UserId userId) {
        if (this.locations.isEmpty()) {
            return;
        }

        this.times_received = 0;

        GeolocationsEvent event = new GeolocationsEvent();
        event.locations = locations;

        ////////////////////// comentado soh para teste //////////////////////
        EventBusIntance.getEventBus().post(event);


        ExecuteTask.executeTask(new TaskDelegator() {
            @Override
            public void doInBackground() throws Exception {

                last_sent = Calendar.getInstance().getTimeInMillis();

                EventMessage message = new EventMessage();

                EventUpdateGeoLocation update = new EventUpdateGeoLocation();

                update.setFromUserId(userId);

                // enviar para a mesma section do usuario
                update.setToUserId(new UserId("", userId.getSector_id()));
                update.setTo(GRXEvent.TO_TYPE.TO_SECTION);
                update.setType(GRXEvent.EVENT_TYPE.REAL_TIME);
                update.setMessage("ponto");

                update.setLocations(locations);

                JSONConverter conv = new JSONConverter();

                message.setEventClass(EventUpdateGeoLocation.class.getCanonicalName());
                message.setJsonString(conv.toJson(update));

                HttpClient.getRef().sendEvent(message);
                locations.clear();
            }

            @Override
            public void onPostExecute() {
                if (super.taskException != null) {
                    MyLog.i("error send location: " + super.taskException.getLocalizedMessage());
                }
            }
        });


    }
}

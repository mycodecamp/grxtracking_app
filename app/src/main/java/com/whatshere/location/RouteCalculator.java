package com.whatshere.location;

import com.google.common.eventbus.EventBus;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.google.maps.model.LatLng;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.eventbus.ProximityEvent;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;

public class RouteCalculator {

    private static float PERCENT_RECALCULATE_ROUTE = 0.8f;
    private static float MAX_DISTANCE_M = 50f;

    private LatLng currentDestination;
    private OrderInfo currentOrder;
    private GoogleMapsServices services;

    private List<OrderInfo> orders;
    private List<OrderInfo> currentOrders;


    private List<com.google.android.gms.maps.model.LatLng> mainRoutePoints;
    private List<com.google.android.gms.maps.model.LatLng> toDrawPoints;
    private EventBus eventBus;

    public RouteCalculator(List<OrderInfo> orders) {
        this.orders = orders;
        this.currentOrders = new ArrayList<>(this.orders);
        this.currentOrder = this.currentOrders.get(0);
        GeoLocation location = this.currentOrder.getAddress().getLocation();
        this.currentDestination = new LatLng(location.getLatitude(), location.getLongitude());
        this.services = new GoogleMapsServices();
        MyLog.i("################# RouteCalculator " + Calendar.getInstance().getTime() + "#################");
    }

    public RouteCalculator(LatLng currentDestination) {
        this.currentDestination = currentDestination;
        this.services = new GoogleMapsServices();
    }

    public void calculateMainRoute(LatLng currentLocation) throws Exception {
        this.mainRoutePoints = this.services.extractRouteInfo(currentLocation, this.currentDestination);
        this.toDrawPoints = this.mainRoutePoints;
        this.eventBus = EventBusIntance.getEventBus();

        MyLog.i("################# calculateMainRoute " + Calendar.getInstance().getTime() + "#################");
    }


    private List<com.google.android.gms.maps.model.LatLng> calculeSubRoute(LatLng location, List<com.google.android.gms.maps.model.LatLng> headRoute) {

        List<com.google.android.gms.maps.model.LatLng> newSubRoute = new ArrayList<>();

        int start_position = 1;
        while (start_position < headRoute.size()) {
            List<com.google.android.gms.maps.model.LatLng> sub_route = headRoute.subList(start_position, headRoute.size());
            if (PolyUtil.isLocationOnPath(new com.google.android.gms.maps.model.LatLng(location.lat, location.lng), sub_route, true, 2f)) {
                start_position += 1;
                MyLog.i("next interation with: " + start_position);
            } else {
                newSubRoute = new ArrayList<>(sub_route);
                newSubRoute.add(0, new com.google.android.gms.maps.model.LatLng(location.lat, location.lng));
                break;
            }
        }

        return newSubRoute;
    }

    /*public void calculateNextOrderDestination(LatLng currentLocation) throws Exception {
        if (!this.currentOrders.isEmpty()) {
            this.currentOrder = this.currentOrders.get(0);
            GeoLocation location = this.currentOrder.getAddress().getLocation();
            this.currentDestination = new LatLng(location.getLatitude(), location.getLongitude());

            this.calculateMainRoute(currentLocation);
        }
    }*/

    public void removeOrderFromRoute(OrderInfo order) {
        if (this.currentOrder.getOrderId().equals(order.getOrderId())) {

            // remove ordem da lista
            this.currentOrders.remove(this.currentOrder);

            if (this.currentOrders.isEmpty()) {
                this.mainRoutePoints = null;
                return;
            }

            // pega seguinte ordem
            this.currentOrder = this.currentOrders.get(0);
            GeoLocation location = this.currentOrder.getAddress().getLocation();
            this.currentDestination = new LatLng(location.getLatitude(), location.getLongitude());

            // clear main route
            this.mainRoutePoints = null;
        } else {
            if (this.currentOrders.contains(order)) {
                this.currentOrders.remove(order);
            }
        }

    }

    public void calculateSubRouteFromLocation(LatLng location) throws Exception {

        double distance = SphericalUtil.computeDistanceBetween(new com.google.android.gms.maps.model.LatLng(this.currentDestination.lat, this.currentDestination.lng), new com.google.android.gms.maps.model.LatLng(location.lat, location.lng));
        MyLog.i("distance to last order: " + distance);
        if (distance < MAX_DISTANCE_M) {

            ProximityEvent event = new ProximityEvent();
            event.location = new GeoLocation();
            event.location.setLatitude(location.lat);
            event.location.setLongitude(location.lng);
            event.order = this.currentOrder;
            event.bringToFront = true;
            this.eventBus.post(event);

            MyLog.i("sending checkin event for order: " + event.order.getOrderId());

            return;
        }


        if (this.toDrawPoints.size() == 2) {
            this.toDrawPoints = this.calculeSubRoute(location, this.toDrawPoints);
            return;
        }

        int break_position = (int) Math.floor(this.toDrawPoints.size() * PERCENT_RECALCULATE_ROUTE);

        List<com.google.android.gms.maps.model.LatLng> headRoute = this.toDrawPoints.subList(0, break_position + 1);
        List<com.google.android.gms.maps.model.LatLng> tailRoute = this.toDrawPoints.subList(break_position + 1, this.toDrawPoints.size());


        // keep original route
        List<com.google.android.gms.maps.model.LatLng> newSubRoute = headRoute;

        try {
            newSubRoute = this.services.extractRouteInfo(location, new LatLng(tailRoute.get(0).latitude, tailRoute.get(0).longitude));
        } catch (Exception e) {
            MyLog.i("error: " + e.getLocalizedMessage());
            newSubRoute = this.calculeSubRoute(location, headRoute);
        }


        MyLog.i("newSubRoute size: " + newSubRoute.size());
        MyLog.i("tailRoute size: " + tailRoute.size());

        this.toDrawPoints = new ArrayList<>(newSubRoute);
        this.toDrawPoints.addAll(tailRoute);


        MyLog.i("################# calculateSubRouteFromLocation " + Calendar.getInstance().getTime() + " #################");
    }

    public List<com.google.android.gms.maps.model.LatLng> getToDrawPoints() {
        return toDrawPoints;
    }

    public List<com.google.android.gms.maps.model.LatLng> getMainRoutePoints() {
        return mainRoutePoints;
    }

    public OrderInfo getCurrentOrder() {
        return currentOrder;
    }
}

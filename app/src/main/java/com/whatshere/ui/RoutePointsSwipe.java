package com.whatshere.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.omega_r.libs.omegarecyclerview.OmegaRecyclerView;
import com.omega_r.libs.omegarecyclerview.swipe_menu.SwipeViewHolder;
import com.whatshere.OrderDetail;
import com.whatshere.R;
import com.whatshere.model.Order;
import com.whatshere.utils.MyExtractor;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.OrderInfo;

/**
 * The Class RoutePoints is a Fragment that is displayed in the RouteInfoActivity activity
 * when the user taps on List tab (2nd tab) or when user swipes to Second page
 * in ViewPager. You can customize this fragment's contents as per your need. It
 * just show a list of dummy places with dummy logo for each place.
 */
@SuppressLint("InflateParams")
public class RoutePointsSwipe extends Fragment {

    /**
     * The place list.
     */
    private ArrayList<Order> pList;

    /**
     * control list
     */

    //private ListView listView;


    private OmegaRecyclerView rv;
    private List<OrderInfo> orders;
    private Hashtable<OrderInfo, Order> mapOrder;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycleviewer, null);

        this.rv = (OmegaRecyclerView) view.findViewById(R.id.recycler_view_orders);

        this.pList = new ArrayList<>();
        this.mapOrder = new Hashtable<>();


        LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.rv.setLayoutManager(llm);
        this.rv.setAdapter(new RoutePointsSwipe.RoutePointListAdapter(new ArrayList<Order>()));

        /*ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                MyLog.i("viewHolder: " + viewHolder);

                int position = viewHolder.getAdapterPosition();

                Order item = pList.get(position);

                pList.remove(item);
                ((RoutePointListAdapter) rv.getAdapter()).notifyDataSetChanged();

                RemoveOrderEvent event = new RemoveOrderEvent();
                event.order = item.getOrder();
                event.notifyToServer = true;

                EventBusIntance.getEventBus().post(event);
            }

        });
        swipeToDismissTouchHelper.attachToRecyclerView(this.rv);*/

        // test();

        return view;
    }


    public void clearOrderList() {
        this.pList = new ArrayList<>();
        ((RoutePointListAdapter) this.rv.getAdapter()).setOrders(this.pList);
        ((RoutePointListAdapter) this.rv.getAdapter()).notifyDataSetChanged();
    }


    public void populateOrderList(List<OrderInfo> orders) {
        this.orders = orders;
        this.pList = new ArrayList<Order>();

        for (OrderInfo order : this.orders) {
            int order_icon = order.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK) ? R.drawable.marker_red_icon : R.drawable.marker_green_icon;
            Order place = new Order("Pedido:" + order.getOrderId(), order.getAddress().getAddress().asAddressString(), 0, order_icon, MyExtractor.extractOrderTypeToString(order));
            place.setOrder(order);
            this.pList.add(place);
            this.mapOrder.put(order, place);
        }

        this.rv.setAdapter(new RoutePointsSwipe.RoutePointListAdapter(this.pList));
    }

    public void removeOrderFromList(OrderInfo order) throws Exception {
        Order o = this.mapOrder.get(order);
        int position = this.pList.indexOf(o);
        this.pList.remove(position);

        this.rv.getAdapter().notifyItemRemoved(position);
        this.rv.getAdapter().notifyItemRangeChanged(position, this.pList.size());
        // PlaceAdapter adapter = (PlaceAdapter) this.listView.getAdapter();
        // adapter.notifyDataSetChanged();

    }


    private class RoutePointListAdapter extends OmegaRecyclerView.Adapter<RoutePointViewHolder> {

        private ArrayList<Order> orders;

        public RoutePointListAdapter(ArrayList<Order> orders) {
            this.orders = orders;
        }

        public ArrayList<Order> getOrders() {
            return orders;
        }

        public void setOrders(ArrayList<Order> orders) {
            this.orders = orders;
        }

        @NonNull
        @Override
        public RoutePointViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            /*View itemView = LayoutInflater.from(getActivity())
                    .inflate(R.layout.place_item, parent, false);*/
            return new RoutePointViewHolder(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull RoutePointViewHolder myholder, int position) {
            // RoutePointViewHolder myholder = (RoutePointViewHolder) holder;
            final Order order = this.orders.get(position);

            myholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), OrderDetail.class);
                    intent.putExtra("order", order.getOrder());
                    startActivity(intent);
                }
            });

            myholder.title.setText(order.getTitle());
            myholder.type.setText(order.getType());
            myholder.address.setText(order.getAddress());
            myholder.img.setImageResource(order.getIcon());
            /*if (order.getOrder().getPeriodType() != null) {
                myholder.period.setText(order.getOrder().getPeriodType().getName());
            }*/
        }

        @Override
        public int getItemCount() {
            return this.orders.size();
        }
    }

    private class RoutePointViewHolder extends SwipeViewHolder {

        protected TextView title;
        protected TextView type;
        protected TextView period;
        protected TextView address;
        protected ImageView img;
        protected View itemView;

        public RoutePointViewHolder(ViewGroup itemView) {
            super(itemView, R.layout.place_item, R.layout.item_right_swipe_menu, R.layout.item_right_swipe_menu);

            this.itemView = itemView;

            title = this.findViewById(R.id.lbl1);
            type = this.findViewById(R.id.lblType);
            period = this.findViewById(R.id.lblPeriod);
            address = (TextView) this.findViewById(R.id.lblAddress);
            img = (ImageView) this.findViewById(R.id.img);
        }
    }


    private void test() {
        List<OrderInfo> orders1 = new ArrayList<>();
        OrderInfo order = new OrderInfo();
        AddressInfo address = new AddressInfo();
        Address add = Address.extractAddressSimple("av  celso garcia, 3964 - Tatuape - Sao Paulo");
        address.setAddress(add);
        order.setAddress(address);

        order.setOrderType(OrderInfo.ORDER_TYPE.EXCHANGE);

        orders1.add(order);
        this.populateOrderList(orders1);
    }
}

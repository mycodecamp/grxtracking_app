package com.whatshere.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.omega_r.libs.omegarecyclerview.OmegaRecyclerView;
import com.whatshere.OrderDetail;
import com.whatshere.R;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.eventbus.RemoveOrderEvent;
import com.whatshere.model.Order;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyExtractor;
import com.whatshere.utils.MyLog;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.OrderInfo;

import static android.support.v7.widget.helper.ItemTouchHelper.ACTION_STATE_SWIPE;

/**
 * The Class RoutePoints is a Fragment that is displayed in the RouteInfoActivity activity
 * when the user taps on List tab (2nd tab) or when user swipes to Second page
 * in ViewPager. You can customize this fragment's contents as per your need. It
 * just show a list of dummy places with dummy logo for each place.
 */
@SuppressLint("InflateParams")
public class RoutePoints extends Fragment {

    /**
     * The place list.
     */
    private ArrayList<Order> pList;

    /**
     * control list
     */

    //private ListView listView;


    private OmegaRecyclerView rv;
    private TextView empty_view;

    private List<OrderInfo> orders;
    private Hashtable<OrderInfo, Order> mapOrder;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recycleviewer, null);
        this.rv = (OmegaRecyclerView) view.findViewById(R.id.recycler_view_orders);
        this.empty_view = view.findViewById(R.id.empty_view);

        this.pList = new ArrayList<>();
        this.mapOrder = new Hashtable<>();


        LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.rv.setLayoutManager(llm);
        this.rv.setAdapter(new RoutePoints.RoutePointListAdapter(new ArrayList<Order>()));

        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            public boolean swipeBack;

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ACTION_STATE_SWIPE) {
                    setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            private void setTouchListener(Canvas c,
                                          RecyclerView recyclerView,
                                          final RecyclerView.ViewHolder viewHolder,
                                          float dX, float dY,
                                          int actionState, boolean isCurrentlyActive) {
                recyclerView.setOnTouchListener(new View.OnTouchListener() {


                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        swipeBack = event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP;
                        if (swipeBack) {
                            final int position1 = viewHolder.getAdapterPosition();
                            if (position1 < 0 || position1 >= pList.size()) {
                                return false;
                            }

                                    final Order item = pList.get(position1);

                            MyDialogs.showConfirm(getActivity(), "Cancelar Pedido", "Deseja cancelar o pedido: " + item.getOrder().getOrderId().getOrder_id() + "?", new MyDialogs.Callback() {
                                @Override
                                public void processYes() {
                                    int position = viewHolder.getAdapterPosition();
                                    Order item = pList.get(position);

                                    RemoveOrderEvent event = new RemoveOrderEvent();
                                    event.order = item.getOrder();
                                    event.notifyToServer = true;

                                    EventBusIntance.getEventBus().post(event);
                                }

                                @Override
                                public void processNo() {
                                    //pList.add(position1, item);
                                    ((RoutePointListAdapter) rv.getAdapter()).notifyDataSetChanged();
                                }
                            });
                        }
                        return false;
                    }
                });
            }

            @Override
            public int convertToAbsoluteDirection(int flags, int layoutDirection) {
                if (swipeBack) {
                    swipeBack = false;
                    return 0;
                }
                return super.convertToAbsoluteDirection(flags, layoutDirection);
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {

            }

        });
        swipeToDismissTouchHelper.attachToRecyclerView(this.rv);

        this.mustToShow();

        return view;
    }


    public void clearOrderList() {
        this.pList = new ArrayList<>();
        ((RoutePointListAdapter) this.rv.getAdapter()).setOrders(this.pList);
        ((RoutePointListAdapter) this.rv.getAdapter()).notifyDataSetChanged();

        this.mustToShow();
    }


    public void populateOrderList(List<OrderInfo> orders) {
        this.orders = orders;
        this.pList = new ArrayList<Order>();

        for (OrderInfo order : this.orders) {
            int order_icon = -1;

            if(order.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK)) {
                order_icon = R.drawable.icone_retira;
            }

            if(order.getOrderType().equals(OrderInfo.ORDER_TYPE.DELIVERY)) {
                order_icon = R.drawable.icone_entrega;
            }

            if(order.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE)) {
                order_icon = R.drawable.icone_troca;
            }


            Order place = new Order("Pedido:" + order.getOrderId(), order.getAddress().getAddress().asAddressString(), 0, order_icon, MyExtractor.extractOrderTypeToString(order));
            place.setOrder(order);
            this.pList.add(place);
            this.mapOrder.put(order, place);
        }

        this.rv.setAdapter(new RoutePoints.RoutePointListAdapter(this.pList));

        this.mustToShow();
    }

    private void mustToShow() {
        try {
            boolean isEmpty = ((RoutePointListAdapter) this.rv.getAdapter()).getOrders().isEmpty();
            if (isEmpty) {
                this.rv.setVisibility(View.GONE);
                this.empty_view.setVisibility(View.VISIBLE);
            } else {
                this.rv.setVisibility(View.VISIBLE);
                this.empty_view.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    public void removeOrderFromList(OrderInfo order) {
        Order o = this.mapOrder.get(order);
        int position = this.pList.indexOf(o);

        if (position == -1) {
            return;
        }

        this.pList.remove(position);
        this.rv.getAdapter().notifyItemRemoved(position);
        this.rv.getAdapter().notifyItemRangeChanged(position, this.pList.size());

        this.mustToShow();
    }


    private class RoutePointListAdapter extends RecyclerView.Adapter {

        private ArrayList<Order> orders;

        public RoutePointListAdapter(ArrayList<Order> orders) {
            this.orders = orders;
        }

        public ArrayList<Order> getOrders() {
            return orders;
        }

        public void setOrders(ArrayList<Order> orders) {
            this.orders = orders;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(getActivity())
                    .inflate(R.layout.place_item, parent, false);
            return new RoutePointViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            RoutePointViewHolder myholder = (RoutePointViewHolder) holder;
            final Order order = this.orders.get(position);

            myholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), OrderDetail.class);
                    intent.putExtra("order", order.getOrder());
                    startActivity(intent);
                }
            });

            myholder.title.setText(order.getTitle());
            myholder.type.setText(order.getType());
            myholder.address.setText(order.getAddress());
            myholder.img.setImageResource(order.getIcon());
            if (order.getOrder().getPeriodType() != null) {
                myholder.period.setText(order.getOrder().getPeriodType().getName());
            }
        }

        @Override
        public int getItemCount() {
            return this.orders.size();
        }
    }

    private class RoutePointViewHolder extends RecyclerView.ViewHolder {

        protected TextView title;
        protected TextView type;
        protected TextView period;
        protected TextView address;
        protected ImageView img;
        protected View itemView;

        public RoutePointViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;

            title = itemView.findViewById(R.id.lbl1);
            type = itemView.findViewById(R.id.lblType);
            period = itemView.findViewById(R.id.lblPeriod);
            address = (TextView) itemView.findViewById(R.id.lblAddress);
            img = (ImageView) itemView.findViewById(R.id.img);
        }
    }


    private void test() {
        List<OrderInfo> orders1 = new ArrayList<>();
        OrderInfo order = new OrderInfo();
        AddressInfo address = new AddressInfo();
        Address add = Address.extractAddressSimple("av  celso garcia, 3964 - Tatuape - Sao Paulo");
        address.setAddress(add);
        order.setAddress(address);

        order.setOrderType(OrderInfo.ORDER_TYPE.EXCHANGE);

        orders1.add(order);
        this.populateOrderList(orders1);
    }
}

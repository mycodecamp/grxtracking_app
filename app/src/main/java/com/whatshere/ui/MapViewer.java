package com.whatshere.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.common.eventbus.Subscribe;
import com.whatshere.OrderDetail;
import com.whatshere.R;
import com.whatshere.custom.CustomFragment;
import com.whatshere.eventbus.RotationEvent;
import com.whatshere.model.Order;
import com.whatshere.persistence.RouteInfoWrapper;
import com.whatshere.utils.MyLog;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;

/**
 * The Class MapViewer is the fragment that shows the Google Map. You
 * can add your code to do whatever you want related to Map functions for your
 * app. For example you can add Map markers here or can show places on map.
 */
public class MapViewer extends CustomFragment implements OnClickListener {

    /**
     * The map view.
     */
    private MapView mMapView;

    /**
     * The Google map.
     */
    private GoogleMap mMap;

    /**
     * The place list.
     */
    private ArrayList<Order> pList;

    /**
     * first time flag
     */
    private boolean firstTime = true;

    /**
     * driver marker
     */
    private Marker markerDriver;

    /**
     * marker table
     */
    private Hashtable<Marker, OrderInfo> tableOrder;
    private Hashtable<OrderInfo, Marker> tableMarker;
    private Polyline lastRoute;


    /* (non-Javadoc)
     * @see android.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.map, null);

        this.tableOrder = new Hashtable<>();
        this.tableMarker = new Hashtable<>();

        initMap(v, savedInstanceState);

        return v;
    }

    /**
     * Initialize the Map view.
     *
     * @param v                  the v
     * @param savedInstanceState the saved instance state object passed from OnCreateView
     *                           method of fragment.
     */
    private void initMap(View v, Bundle savedInstanceState) {
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                // mMap.getUiSettings().setTiltGesturesEnabled(true);
                // mMap.getUiSettings().setAllGesturesEnabled(true);
                // mMap.getUiSettings().setZoomControlsEnabled(false);
                // mMap.setMyLocationEnabled(true);
                // setupMapMarkers();

                mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
                mMap.getUiSettings().setCompassEnabled(false);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(MapViewer.this.getActivity(), R.raw.map_json_style));
                mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

                mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(getActivity(), OrderDetail.class);
                        intent.putExtra("order", tableOrder.get(marker));
                        startActivity(intent);
                    }
                });

                mMap.addTileOverlay(new TileOverlayOptions().tileProvider(new TileProvider() {
                    @Override
                    public Tile getTile(int i, int i1, int i2) {
                        return NO_TILE;
                    }
                }));

                /* mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        GeolocationsEvent event = new GeolocationsEvent();
                        List<GeoLocation> locations = new ArrayList<>();
                        GeoLocation geo = new GeoLocation();
                        geo.setLatitude(latLng.latitude);
                        geo.setLongitude(latLng.longitude);
                        locations.add(geo);
                        event.locations = locations;
                        EventBusIntance.getEventBus().post(event);
                        MyLog.i("location notified");
                    }
                }); */

            }
        });
        mMapView.onCreate(savedInstanceState);
    }

    public void showOrderList(RouteInfo routeInfo) {
    }

    public void cleanRouteInfo() {
        if (mMap != null) {
            mMap.clear();
        }
    }


    public void drawRouteFromPoints(List<LatLng> toDrawPoints) {
        if (this.lastRoute != null) {
            this.lastRoute.remove();
        }

        PolylineOptions polyline = new PolylineOptions();
        for (com.google.android.gms.maps.model.LatLng latlng : toDrawPoints) {
            polyline.add(latlng);
        }
        polyline.width(8).color(MapViewer.this.getResources().getColor(R.color.route_color)).geodesic(true);

        this.lastRoute = mMap.addPolyline(polyline);
    }


    public void drawRoute(RouteInfoWrapper routeInfo) {
        try {
            if (mMap != null) {

                GeoLocation firstPoint = null;

                for (RoutePointInfo point : routeInfo.getPoints()) {
                    GoogleRouteInfo googleRoute = point.getGoogleRoute();
                    List<GeoLocation> locations = googleRoute.getLocationRepresentation();

                    PolylineOptions polyline = new PolylineOptions().width(5).color(MapViewer.this.getResources().getColor(R.color.route_color)).geodesic(true);
                    for (int index = 0; index < locations.size(); index++) {
                        GeoLocation location = locations.get(index);

                        if (firstPoint == null) {
                            firstPoint = location;
                        }

                        LatLng point2 = new LatLng(location.getLatitude(), location.getLongitude());
                        polyline.add(point2);
                    }
                    mMap.addPolyline(polyline).setZIndex(0);

                }

                mMap.animateCamera(createCameraAnimation(firstPoint));
            }
        } catch (Exception e) {
            MyLog.log(e);
        }
    }


    private MarkerOptions createMarkerDriver(GeoLocation location, float bearing) {
        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.seta_azul);
        MarkerOptions marker = new MarkerOptions().position(latlng).anchor(0.5f, 0.5f).rotation(bearing).icon(icon);
        return marker;
    }


    private CameraUpdate createCameraAnimation(GeoLocation location) {
        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latlng)      // Sets the center of the map to Mountain View
                .zoom(13)                   // Sets the zoom
                //   .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();

        return CameraUpdateFactory.newCameraPosition(cameraPosition);
    }


    public void removeOrderMarker(OrderInfo order) {
        Marker marker = this.tableMarker.get(order);

        MyLog.i("marker: " + marker);

        if (marker != null) {
            marker.remove();
        }
    }

    public void drawOrders(List<OrderInfo> orders) {
        try {
            if (mMap != null) {
                for (OrderInfo order : orders) {

                    GeoLocation orderLocation = order.getAddress().getLocation();

                    LatLng latlng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());

                    BitmapDescriptor icon = null;
                    if (order.getOrderType().equals(OrderInfo.ORDER_TYPE.DELIVERY)) {
                        icon = BitmapDescriptorFactory.fromResource(R.drawable.icone_entrega);
                    }

                    if (order.getOrderType().equals(OrderInfo.ORDER_TYPE.EXCHANGE)) {
                        icon = BitmapDescriptorFactory.fromResource(R.drawable.icone_troca);
                    }

                    if (order.getOrderType().equals(OrderInfo.ORDER_TYPE.PICK)) {
                        icon = BitmapDescriptorFactory.fromResource(R.drawable.icone_retira);
                    }

                    Marker marker = mMap.addMarker(new MarkerOptions().position(latlng).icon(icon).title("Pedido: " + order.getOrderId()).snippet(order.getAddress().getAddress().asAddressString()));
                    marker.setZIndex(50f);
                    tableOrder.put(marker, order);
                    tableMarker.put(order, marker);
                }
            }
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    public void updateMapLocation(List<GeoLocation> locations) {
        try {
            if (mMap != null) {
                if (this.markerDriver != null) {
                    this.markerDriver.remove();
                }

                GeoLocation first = locations.get(0);
                this.markerDriver = mMap.addMarker(createMarkerDriver(first, this.last_bearing));
                this.markerDriver.setZIndex(100);

                if (firstTime) {
                    mMap.animateCamera(createCameraAnimation(first));
                    firstTime = false;
                }
            }
        } catch (Exception e) {
            MyLog.log(e);
        }
    }

    private float last_bearing = 0.0f;

    @Subscribe
    public void processBearing(RotationEvent event) {
        try {
            //   MyLog.i("processBearing: " + event.bearing);
            this.last_bearing = event.bearing;

            if (this.markerDriver != null) {
                this.markerDriver.setRotation(this.last_bearing);
            }

        } catch (Exception e) {
            MyLog.log(e);
        }
    }


    /**
     * This class creates a Custom a InfoWindowAdapter that is used to show
     * popup on map when user taps on a pin on the map. Current implementation
     * of this class will show a Title and a snippet with one static image.
     */
    private class CustomInfoWindowAdapter implements InfoWindowAdapter {

        /**
         * The contents view.
         */
        private final View mContents;

        /**
         * Instantiates a new custom info window adapter.
         */
        @SuppressLint("InflateParams")
        public CustomInfoWindowAdapter() {
            mContents = getActivity().getLayoutInflater().inflate(
                    R.layout.map_popup, null);
            mContents.getBackground().setColorFilter(getResources().getColor(R.color.main_select_theme),
                    PorterDuff.Mode.SRC_ATOP);
        }

        /* (non-Javadoc)
         * @see com.google.android.gms.maps.GoogleMap.InfoWindowAdapter#getInfoWindow(com.google.android.gms.maps.model.Marker)
         */
        @Override
        public View getInfoWindow(Marker marker) {
            render(marker, mContents);
            return mContents;
        }

        /* (non-Javadoc)
         * @see com.google.android.gms.maps.GoogleMap.InfoWindowAdapter#getInfoContents(com.google.android.gms.maps.model.Marker)
         */
        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        /**
         * Render the marker content on Popup view. Customize this as per your
         * need.
         *
         * @param marker the marker
         * @param view   the content view
         */
        private void render(final Marker marker, View view) {

            String title = marker.getTitle();
            TextView titleUi = (TextView) view.findViewById(R.id.title);
            if (title != null) {
                SpannableString titleText = new SpannableString(title);
                titleText.setSpan(new ForegroundColorSpan(Color.WHITE), 0,
                        titleText.length(), 0);
                titleUi.setText(titleText);
            } else {
                titleUi.setText("");
            }

            String snippet = marker.getSnippet();
            TextView snippetUi = (TextView) view.findViewById(R.id.snippet);
            if (snippet != null) {
                SpannableString snippetText = new SpannableString(snippet);
                snippetText.setSpan(new ForegroundColorSpan(getResources()
                                .getColor(R.color.main_color_theme)), 0, snippet.length(),
                        0);
                snippetUi.setText(snippetText);
            } else {
                snippetUi.setText("");
            }

        }
    }

    /* (non-Javadoc)
     * @see android.app.Fragment#onResume()
     */
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();

        // mMap = mMapView.getMap();
        if (mMap != null) {
            //   mMap.setMyLocationEnabled(true);
            mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
            // setupMapMarkers();
            mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

                @Override
                public void onInfoWindowClick(Marker marker) {
                    Intent intent = new Intent(getActivity(), OrderDetail.class);
                    intent.putExtra("order", tableOrder.get(marker));
                    startActivity(intent);
                }
            });
        }
    }

    /* (non-Javadoc)
     * @see android.app.Fragment#onPause()
     */
    @Override
    public void onPause() {

        mMapView.onPause();
        if (mMap != null)
            mMap.setInfoWindowAdapter(null);
        super.onPause();
    }

    /* (non-Javadoc)
     * @see android.app.Fragment#onDestroy()
     */
    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    /* (non-Javadoc)
     * @see android.app.Fragment#onLowMemory()
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /* (non-Javadoc)
     * @see android.app.Fragment#onSaveInstanceState(android.os.Bundle)
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {

    }

}

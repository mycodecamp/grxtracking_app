package com.whatshere;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.google.common.eventbus.Subscribe;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.whatshere.http.HttpClient;
import com.whatshere.persistence.DAO;
import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.utils.MyDialogs;
import com.whatshere.utils.MyLog;

import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.TokenInfo;

/**
 * The Class SplashScreen will launched at the start of the application. It will
 * be displayed for 3 seconds and than finished automatically and it will also
 * start the next activity of app.
 */
public class SplashScreen extends Activity {

    /**
     * Check if the app is running.
     */
    private boolean isRunning;

    private DAO dao;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        {
            String androidId = android.provider.Settings.Secure.getString(getContentResolver(),
                    android.provider.Settings.Secure.ANDROID_ID);
            DAO.AndroidID = androidId;
            MyLog.i("### AndroidID: " + DAO.AndroidID + " ###");
        }

        this.dao = new DAO();

        FirebaseApp.initializeApp(this);

        setContentView(R.layout.splash);

        isRunning = true;

        startSplash();

        EventBusIntance.getEventBus().register(new Object() {
            @Subscribe
            public void registerToken(TokenInfo token) {
                sentToken(token);
            }
        });


    }

    private void sentToken(final TokenInfo token) {
        ExecuteTask.executeTask(new TaskDelegator() {
            @Override
            public void doInBackground() throws Exception {
                MyLog.i("sending token:" + token.getTokenInfo());
                HttpClient.getRef().registerTokenFirebase(token);
            }
        });

    }

    /**
     * Starts the count down timer for 3-seconds. It simply sleeps the thread
     * for 3-seconds.
     */
    private void startSplash() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Thread.sleep(1000);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            doFinish();
                        }
                    });
                }
            }
        }).start();
    }

    private void checkStoreUser() {
        this.dao.retrieveObject("userinfo", UserInfo.class, new DAO.Callback<UserInfo>() {
            @Override
            public void retrieve(UserInfo user) {
                if (user == null) {
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } else {
                    final Credentials credentials = new Credentials();
                    credentials.setUsername(user.getUserId().getUser_id());
                    credentials.setPassword(user.getPassword());
                    credentials.setApp_version("v1.0.6");

                    ExecuteTask.executeTask(new TaskDelegator() {
                        @Override
                        public void doInBackground() throws Exception {
                            UserInfo result = HttpClient.getRef().loginWithUser(credentials, dao);
                            result.setRoles(null);

                            String tokenStr = FirebaseInstanceId.getInstance().getToken();
                            TokenInfo token = new TokenInfo();
                            token.setTokenInfo(tokenStr);
                            sentToken(token);

                            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                            Bundle extras = new Bundle();
                            extras.putSerializable("userinfo", result);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtras(extras);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onPostExecute() {
                            if (taskException != null) {
                                MyLog.log(taskException);
                                MyDialogs.showError(SplashScreen.this, "Erro no login", taskException.getLocalizedMessage());
                                dao.removeObject("userinfo");
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * If the app is still running than this method will start the Login
     * activity and finish the Splash.
     */
    private synchronized void doFinish() {

        if (isRunning) {
            isRunning = false;
            this.checkStoreUser();
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isRunning = false;
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
package com.whatshere;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import com.whatshere.eventbus.EventBusIntance;
import com.whatshere.eventbus.RotationEvent;

import java.util.Calendar;

class SensorEventListener implements android.hardware.SensorEventListener {


    float[] gData = new float[3]; // accelerometer
    float[] mData = new float[3]; // magnetometer
    float[] rMat = new float[9];
    float[] iMat = new float[9];
    float[] orientation = new float[3];
    private int mAzimuth = 0; // degree

    private static final int FROM_RADS_TO_DEGS = -57;

    private float[] mGravity;
    private float[] mGeomagnetic;

    private long last;

    public SensorEventListener() {
        this.last = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {


        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                float azimut = orientation[0]; // orientation contains: azimut, pitch and roll
                float pitch = orientation[1];
                float roll = orientation[2];

                float degrees = Float.parseFloat(Math.toDegrees(azimut) + "");
                // MyLog.i("g: " + degrees);

                {
                    if (Calendar.getInstance().getTimeInMillis() - this.last > 5000) {
                        if (degrees < 0) {
                            degrees = 360 + degrees;
                        }
                        RotationEvent event1 = new RotationEvent();
                        event1.bearing = degrees;
                        EventBusIntance.getEventBus().post(event1);
                        this.last = Calendar.getInstance().getTimeInMillis();
                    }

                }

            }
        }
    }

    private void update(float[] vectors) {
        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, vectors);
        int worldAxisX = SensorManager.AXIS_X;
        int worldAxisZ = SensorManager.AXIS_Z;
        float[] adjustedRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisX, worldAxisZ, adjustedRotationMatrix);
        float[] orientation = new float[3];
        SensorManager.getOrientation(adjustedRotationMatrix, orientation);
        float pitch = orientation[1] * FROM_RADS_TO_DEGS;
        float roll = orientation[2] * FROM_RADS_TO_DEGS;

    }

}

package com.whatshere;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.hardware.Camera;
import android.hardware.Camera.ShutterCallback;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.whatshere.tasks.ExecuteTask;
import com.whatshere.tasks.TaskDelegator;
import com.whatshere.utils.MyLog;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.List;

public class SurfaceCameraActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private ResultPictureListener listener;
    private SurfaceView surface;
    private SurfaceHolder holder;
    private Camera camera;
    private String from;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surface_camera);

        ImageButton take_photo = findViewById(R.id.btn_take_picture);
        this.progressBar = (ProgressBar) findViewById(R.id.indeterminateBar);
        this.progressBar.setMax(10);

        this.progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.main_select_theme), PorterDuff.Mode.SRC_IN);





        ImageView cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  finish();
                Camera.Parameters params = camera.getParameters();
                int current_zoom = params.getZoom();
                current_zoom -= 10;

                if (current_zoom < 0) {
                    return;
                }

                Toast.makeText(SurfaceCameraActivity.this, "zoom:" + current_zoom, Toast.LENGTH_LONG).show();

                params.setZoom(current_zoom);

                camera.setParameters(params);
            }
        });

        ImageView accept = findViewById(R.id.btn_accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  finish();
                Camera.Parameters params = camera.getParameters();
                int current_zoom = params.getZoom();
                current_zoom += 10;
                if (current_zoom > 60) {
                    return;
                }

                Toast.makeText(SurfaceCameraActivity.this, "zoom:" + current_zoom, Toast.LENGTH_LONG).show();

                params.setZoom(current_zoom);

                camera.setParameters(params);
            }
        });

        TextView message = findViewById(R.id.textview_picture_message);

        Intent intent = getIntent();
        if (intent != null && intent.getStringExtra("message") != null) {
            message.setText(intent.getStringExtra("message"));
        }

        this.surface = findViewById(R.id.surface_camera);
        this.holder = this.surface.getHolder();
        this.holder.addCallback(this);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }


        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new ShutterCallback() {
                    public void onShutter() {

                    }
                };
                camera.takePicture(new ShutterCallback() {
                    public void onShutter() {
                        MyLog.i("onShutter");
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes, Camera camera) {
                        MyLog.i("onPictureTaken");

                    }
                }, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes, Camera camera) {
                        try {
                            MyLog.i("onPictureTaken2");
                            camera.stopPreview();
                            camera.release();

                            BitmapFactory.Options opt = new BitmapFactory.Options();
                            Bitmap tmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);

                            MyLog.i("picture w: " + tmp.getWidth());
                            MyLog.i("picture h: " + tmp.getHeight());

                            float ratio_w = 0.10f;
                            float ratio_h = 0.70f;

                            float w = ratio_w * tmp.getWidth();
                            float h = ratio_h * tmp.getHeight();


                            float x = (tmp.getWidth() / 2) - w / 2;
                            float y = (tmp.getHeight() - h) / 2;


                            MyLog.i("picture newW: " + w);
                            MyLog.i("picture newH: " + h);


                            Bitmap cropedBitmap = Bitmap.createBitmap(tmp, (int) x, (int) y, (int) w, (int) h);
                            if (listener != null) {
                                listener.result(cropedBitmap, from);
                            }

                            {


                                ExecuteTask.executeTask(new TaskDelegator() {
                                    @Override
                                    public void doInBackground() throws Exception {
                                       //  TGHttpClient.getRef().sendImageInfo(image);
                                        finish();
                                    }


                                    @Override
                                    public void onPostExecute() {
                                        Toast.makeText(SurfaceCameraActivity.this, "Imagem enviada com sucesso!", Toast.LENGTH_LONG).show();

                                        progressBar.setVisibility(View.GONE);
                                    }
                                });


                            }


                        } catch (Exception e) {
                            MyLog.log(e);
                        }
                    }
                });
            }
        });


    }

    public Camera.Size determineBestPreviewSize(Camera.Parameters parameters) {
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        MyLog.i("preview sizes");
        for (Camera.Size s : sizes) {
            MyLog.i("size w: " + s.width + " h: " + s.height);
        }
        return determineBestSize(sizes);
    }

    public Camera.Size determineBestPictureSize(Camera.Parameters parameters) {
        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
        MyLog.i("picture sizes");
        for (Camera.Size s : sizes) {
            MyLog.i("size w: " + s.width + " h: " + s.height);
        }
        return determineBestSize(sizes);
    }

    protected Camera.Size determineBestSize(List<Camera.Size> sizes) {
        Camera.Size bestSize = null;
        long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long availableMemory = Runtime.getRuntime().maxMemory() - used;
        for (Camera.Size currentSize : sizes) {
            int newArea = currentSize.width * currentSize.height;
            long neededMemory = newArea * 4 * 4; // newArea * 4 Bytes/pixel * 4 needed copies of the bitmap (for safety :) )
            boolean isDesiredRatio = (currentSize.width / 4) == (currentSize.height / 3);
            boolean isBetterSize = (bestSize == null || currentSize.width > bestSize.width);
            boolean isSafe = neededMemory < availableMemory;
            if (isDesiredRatio && isBetterSize && isSafe) {
                bestSize = currentSize;
            }
        }
        if (bestSize == null) {
            return sizes.get(0);
        }
        return bestSize;
    }

    public void setListener(ResultPictureListener listener) {
        this.listener = listener;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        MyLog.i("surfaceCreated requering Camera.open");

        this.camera = Camera.open(getFrontCameraId());

        MyLog.i("camera: " + this.camera);

        Camera.Parameters param;
        param = this.camera.getParameters();

        // Toast.makeText(this, "camera zoom: " + param.getZoom() + " max zoom: " + param.getMaxZoom(), Toast.LENGTH_LONG).show();

        param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        param.setZoom(0);

        {
            Camera.Size pictureSize = determineBestPictureSize(this.camera.getParameters());
            param.setPictureSize(pictureSize.width, pictureSize.height);

            MyLog.i("picture size w:" + pictureSize.width + " h: " + pictureSize.height);

            Camera.Size previewSize = determineBestPreviewSize(this.camera.getParameters());
            param.setPreviewSize(previewSize.width, previewSize.height);

            MyLog.i("preview size w:" + previewSize.width + " h: " + previewSize.height);
        }
        {
            //STEP #1: Get rotation degrees
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);

            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break; //Natural orientation
                case Surface.ROTATION_90:
                    degrees = 90;
                    break; //Landscape left
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;//Upside down
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;//Landscape right
            }
            int rotate = (info.orientation - degrees + 360) % 360;
            param.setRotation(rotate);
        }


        this.camera.setParameters(param);
        this.camera.setDisplayOrientation(90);

        try {
            this.camera.setPreviewDisplay(this.holder);
            this.camera.startPreview();
        } catch (Exception e) {
            MyLog.log(e);
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public static interface ResultPictureListener {
        public void result(Bitmap bitmap, String from);

    }


    public int getFrontCameraId() {
        if (Build.VERSION.SDK_INT < 22) {
            Camera.CameraInfo ci = new Camera.CameraInfo();
            for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                Camera.getCameraInfo(i, ci);
                if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    return Camera.CameraInfo.CAMERA_FACING_BACK;
                }
            }
        } else {
            try {
                CameraManager cManager = (CameraManager) getApplicationContext()
                        .getSystemService(Context.CAMERA_SERVICE);
                String[] cameraId = cManager.getCameraIdList();
                for (int j = 0; j < cameraId.length; j++) {
                    CameraCharacteristics characteristics = cManager.getCameraCharacteristics(cameraId[j]);
                    int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                    if (cOrientation == CameraCharacteristics.LENS_FACING_BACK) {
                        return Integer.parseInt(cameraId[j]);
                    }
                }
            } catch (CameraAccessException e) {
                MyLog.log(e);
            }
        }
        return -1;
    }

}

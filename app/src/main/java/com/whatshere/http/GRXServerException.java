package com.whatshere.http;

public class GRXServerException extends  Exception {

    public GRXServerException(String message) {
        super(message);
    }
}

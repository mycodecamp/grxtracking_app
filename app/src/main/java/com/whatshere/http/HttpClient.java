package com.whatshere.http;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whatshere.persistence.DAO;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyLog;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import grx.tracking.core.persistence.ContainerInfo;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RouteStatInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.core.transport.TokenInfo;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpClient {

    private static Logger logger = Logger.getLogger(HttpClient.class.getCanonicalName());

    // private static String URL = "http://grxtracking.ddns.net/trackingrest";
    private static String URL = "http://grxtrackingserver.ddns.net/grxtrackingrest/v1";
    // private static String URL = "http://grxtracking.ddns.net:9090/grxtrackingrest/v1";

    private static HttpClient client;

    public static HttpClient getRef() {
        if (client == null) {
            client = new HttpClient();
        }
        return client;
    }


    private JSONConverter conv;

    private String token;
    private Date lastLogin;

    private HttpClient() {
        this.conv = new JSONConverter();
    }


    private Response post(String endpoint, String json) throws Exception {
        OkHttpClient client = new OkHttpClient();
        RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

        Request request = new Request.Builder().url(URL + endpoint).post(post)
                .addHeader("Authorization", "Bearer " + this.token).build();

        Response response = client.newCall(request).execute();

        if (response.code() == 403) {
            String message = response.body().string() + " - logar novamente.";
            throw new GRXServerException(message);
        }

        return response;
    }

    private <T> Response post(String endpoint, Object obj) throws Exception {
        String json = this.conv.toJson(obj);
        return this.post(endpoint, json);
    }

    private Response get(String endpoint) throws Exception {
        OkHttpClient client = new OkHttpClient();
        // RequestBody get = RequestBody.create(MediaType.parse("application/json;
        // charset=utf-8"), json);

        Request request = new Request.Builder().url(URL + endpoint).addHeader("Authorization", "Bearer " + this.token)
                .header("Content-Length", "0").build();

        Response response = client.newCall(request).execute();

        return response;
    }

    private Response get(String endpoint, Hashtable<String, String> params) throws Exception {
        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder httpBuider = HttpUrl.parse(URL + endpoint).newBuilder();

        for (String key : params.keySet()) {
            httpBuider.addQueryParameter(key, params.get(key));
        }

        HttpUrl url = httpBuider.build();

        Request request = new Request.Builder().url(url).addHeader("Authorization", "Bearer " + this.token)
                .header("Content-Length", "0").build();

        Response response = client.newCall(request).execute();

        return response;
    }


    private Response get(String endpoint, Object obj) throws Exception {
        String json = this.conv.toJson(obj);
        return this.get(endpoint, json);
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserInfo loginWithUser(Credentials user, DAO dao) throws Exception {
        Response response = this.post("/authentication/user", user);
        if (response.isSuccessful()) {
            String json_response = response.body().string();

            logger.info("json response: " + json_response);

            UserInfo user1 = this.conv.toObject(json_response, UserInfo.class);

            this.token = user1.getCurrentToken();
            this.lastLogin = user1.getLastLogin();

            dao.saveObj("last_token", this.token);


            MyLog.i("this.token:" + this.token);

            return user1;
        } else {
            throw new Exception(response.message() + ": " + response.body().string());
        }
    }

    public void registerTokenFirebase(TokenInfo token) throws Exception {
        Response response = this.post("/authentication/registertoken", token);
        if (response.isSuccessful()) {
            String json_response = response.body().string();

            logger.info("json response: " + json_response);

        } else {
            throw new Exception(response.message() + ": " + response.body().string());
        }
    }

    private void asyncPost(String endpoint, String json, Callback callback) throws Exception {
        OkHttpClient client = new OkHttpClient.Builder().readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES).build();
        RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

        Request request = new Request.Builder().url(URL + endpoint).post(post)
                .addHeader("Authorization", "Bearer " + this.token).build();

        client.newCall(request).enqueue(callback);
    }


    public RouteInfo getRoute(String orderId) throws Exception {

        Hashtable<String, String> params = new Hashtable<>();
        params.put("order_id", orderId);
        Response response = this.get("/authentication/getroute", params);

        String jsonString = response.body().string();

        RouteInfo route = this.conv.toObject(jsonString, RouteInfo.class);
        return route;
    }

    public RouteStatInfo getPayment(String route_id) throws Exception{
        Hashtable<String, String> params = new Hashtable<>();
        params.put("route_id", route_id);

        Response response = this.get("/dataprovider/paymentinfo", params);

        String jsonString = response.body().string();

        MyLog.i("order from server: " + jsonString);

        RouteStatInfo payment = this.conv.toObject(jsonString, RouteStatInfo.class);

        return payment;
    }

    public List<OrderInfo> getRouteOrders(String orderId) throws Exception {

        Hashtable<String, String> params = new Hashtable<>();
        params.put("order_id", orderId);

        Response response = this.get("/authentication/getrouteorders", params);

        String jsonString = response.body().string();

        MyLog.i("order from server: " + jsonString);

        OrderInfo[] orders = this.conv.toObject(jsonString, OrderInfo[].class);

        List<OrderInfo> ll = Arrays.asList(orders);
        for (OrderInfo order : orders) {
            MyLog.i("address:" + order.getAddress().getAddress().asAddressString());
            MyLog.i("xbairro: " + order.getAddress().getAddress().getXBairro());
        }
        return ll;
    }


    public List<ContainerInfo> getContainers(String sectorId) throws Exception {

        Hashtable<String, String> params = new Hashtable<>();
        params.put("sector_id", sectorId);

        Response response = this.get("/dataprovider/containerinfo", params);

        String jsonString = response.body().string();

        ContainerInfo[] containers = this.conv.toObject(jsonString, ContainerInfo[].class);
        return Arrays.asList(containers);
    }

    public List<TruckInfo> getTrucks(UserId userId) throws Exception {

        Hashtable<String, String> params = new Hashtable<>();
        params.put("user_id", userId.getUser_id());
        params.put("sector_id", userId.getSector_id());

        Response response = this.get("/dataprovider/trucksinfo", params);

        String jsonString = response.body().string();

        TruckInfo[] trucks = this.conv.toObject(jsonString, TruckInfo[].class);
        return Arrays.asList(trucks);

    }

    public void asyncPost(String endpoint, Object obj, Callback callback) throws Exception {
        String json = this.conv.toJson(obj);
        this.asyncPost(endpoint, json, callback);
    }

    public void sendEvent(EventMessage message) throws Exception {
        this.post("/grxevents/update", message);
    }

    public List<SectorInfo> getBases() throws Exception {

        Hashtable<String, String> params = new Hashtable<>();

        Response response = this.get("/dataprovider/basesinfo", params);

        String jsonString = response.body().string();

        System.out.println("jsonString: " + jsonString);

        SectorInfo[] sectors = this.conv.toObject(jsonString, SectorInfo[].class);
        return Arrays.asList(sectors);
    }



    /*
     * public void updateGeoLocation(GeoLocationUpdate update) throws Exception { //
     * this.post("/geolocation/update", locations);
     * this.asyncPost("/geolocation/update", update, new Callback() {
     *
     * public void onResponse(Call call, Response response) throws IOException {
     * System.out.println(response.body().string()); }
     *
     * public void onFailure(Call call, IOException io) {
     * System.out.println(io.getMessage());
     *
     * } }); System.out.println("called"); //
     * System.out.println(response.body().string()); }
     */
}

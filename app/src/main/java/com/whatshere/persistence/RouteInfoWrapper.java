package com.whatshere.persistence;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;

public class RouteInfoWrapper implements Serializable {

    private RouteInfo route;
    private List<RoutePointInfo> points;

    public RouteInfo getRoute() {
        return route;
    }

    public void setRoute(RouteInfo route) {
        this.route = route;
    }

    public List<RoutePointInfo> getPoints() {
        return points;
    }

    public void setPoints(List<RoutePointInfo> points) {
        this.points = points;
    }

    public static RouteInfoWrapper wrap(RouteInfo route) {
        RouteInfoWrapper wrapper = new RouteInfoWrapper();
        wrapper.setRoute(route);
        List<RoutePointInfo> points1 = Arrays.asList(route.getPoints());
        wrapper.setPoints(points1);
        route.setPoints(null);

        return wrapper;
    }
}

package com.whatshere.persistence;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.whatshere.utils.JSONConverter;
import com.whatshere.utils.MyLog;

import java.util.List;

import grx.tracking.core.persistence.ContainerInfo;
import grx.tracking.core.persistence.OrderInfo;

public class DAO {

    public static String AndroidID;

    private static FirebaseDatabase database;

    private static FirebaseDatabase getDB() {
        if (database == null) {
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
        }

        return database;
    }

    public DAO() {
        this.database = getDB();
    }

    private DatabaseReference getDeviceRef() {
        DatabaseReference ref = database.getReference(AndroidID);
        ref.keepSynced(true);
        return ref;
    }

    public void removeObject(String name) {
        getDeviceRef().child(name).removeValue();
    }

    public void saveAsJSON(String name, Object obj) {
        try {
            JSONConverter conv = new JSONConverter();
            String json = conv.toJson(obj);
            this.saveObj(name, json);
        } catch (Exception e) {
            MyLog.log(e);
        }
    }


    public void saveObj(String name, Object obj) {
        getDeviceRef().child(name).setValue(obj);
    }

    public <T> void retrieveAsListObject(String name, final Class<T> classs, final Callback<T> callback) {

        getDeviceRef().child(name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<List<OrderInfo>> t = new GenericTypeIndicator<List<OrderInfo>>() {
                };

                List<OrderInfo> obj = dataSnapshot.getValue(t);
                callback.retrieve(obj);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public <T> void retrieveJSONObject(String name, final Class<T> classs, final Callback<T> callback) {
        this.retrieveObject(name, String.class, new Callback<String>() {
            @Override
            public void retrieve(String obj) {
                try {
                    JSONConverter conv = new JSONConverter();
                    T obj1 = conv.toObject(obj, classs);
                    callback.retrieve(obj1);
                } catch (Exception e) {
                    MyLog.log(e);
                }
            }
        });

    }

    public <T> void retrieveObject(String name, final Class<T> classs, final Callback<T> callback) {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                T obj = dataSnapshot.getValue(classs);
                callback.retrieve(obj);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        getDeviceRef().child(name).addValueEventListener(listener);


    }

    public void close() {
        // database.
    }

    public static class Callback<T> {
        public void retrieve(T obj) {
        }

        ;

        public void retrieve(List obj) {
        }

        ;
    }
}
